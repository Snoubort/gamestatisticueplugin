#include "EnemyCsvRow.h"

UEnemyCsvRow::UEnemyCsvRow(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	rowType = ECsvRowTypes::SpellRow;
}

void UEnemyCsvRow::SetEnemyId(int inEnemyId)
{
	if (inEnemyId <= 0) {
		UE_LOG(LogStatisticCollector, Error, TEXT("Enemy ID is incorrect"));
		return;
	}
	enemyId = inEnemyId;
}

int UEnemyCsvRow::GetEnemyId()
{
	return 0;
}

void UEnemyCsvRow::SetRoundNumber(int inRoundNumber)
{
	if (inRoundNumber <= 0) {
		UE_LOG(LogStatisticCollector, Error, TEXT("Round number is incorrect"));
		return;
	}
	roundNumber = inRoundNumber;
}

int UEnemyCsvRow::GetRoundNumber()
{
	return 0;
}

void UEnemyCsvRow::SetSpawnTime(FDateTime inSpawnTime)
{
	spawnTime = inSpawnTime;
}

FDateTime UEnemyCsvRow::GetSpawnTime()
{
	return FDateTime();
}

void UEnemyCsvRow::SetDeathTime(FDateTime inDeathTime)
{
	deathTime = inDeathTime;
}

FDateTime UEnemyCsvRow::GetDeathTime()
{
	return FDateTime();
}

void UEnemyCsvRow::SetEnemyType(FString inEnemyType)
{
	enemyType = inEnemyType;
}

FString UEnemyCsvRow::GetEnemyType()
{
	return enemyType;
}

FString UEnemyCsvRow::GetDataString()
{
	FString id = FString::FromInt(enemyId);
	FString round = FString::FromInt(roundNumber);
	FString outSpawnTime = spawnTime.ToString();
	FString outDeathTime = deathTime.ToString();
	FString outSpawnPoint = spawnPoint.ToString();
	FString outDeathPoint = deathPoint.ToString();
	return FString(id + ';' + round + ';' + outSpawnTime + ';' + outDeathTime +
		';' + outSpawnPoint + ';' + outDeathPoint + ';' + enemyType + '\n');
}

FString UEnemyCsvRow::GetHeaderString()
{
	return "Enemy ID;Round Number;Spawn time;Death time;Spawn point;Death Point;EnemyType\n";
}