#include "SpellCsvRow.h"

USpellCsvRow::USpellCsvRow(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	rowType = ECsvRowTypes::SpellRow;
}

void USpellCsvRow::SetRoundNum(int round)
{
	if (round <= 0) {
		UE_LOG(LogStatisticCollector, Error, TEXT("Round number is incorrect"));
		return;
	}
	roundNum = round;
}

int USpellCsvRow::GetRoundNum()
{
	return roundNum;
}

void USpellCsvRow::SetUsingTime(FDateTime time)
{
	usingTime = time;
}

FDateTime USpellCsvRow::GetUsingTime()
{
	return usingTime;
}

void USpellCsvRow::SetUsingPoint(FVector point)
{
	usingPoint = point;
}

FVector USpellCsvRow::GetUsingPoint()
{
	return usingPoint;
}

void USpellCsvRow::SetSpellType(FString type)
{
	spellType = type;
}

FString USpellCsvRow::GetSpellType()
{
	return spellType;
}

FString USpellCsvRow::GetDataString()
{
	FString round = FString::FromInt(roundNum);
	FString time = usingTime.ToString();
	FString point = usingPoint.ToString();
	return FString(round + ';' + time + ';' + point + ';' + spellType + '\n');
}

FString USpellCsvRow::GetHeaderString()
{
	return "Round number;Using time;Using point;Spell Type\n";
}
