#include "TowerCsvRow.h"

UTowerCsvRow::UTowerCsvRow(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	rowType = ECsvRowTypes::SpellRow;
}

void UTowerCsvRow::SetTowerId(int inTowerId)
{
	if (inTowerId < 0) {
		UE_LOG(LogStatisticCollector, Error, TEXT("Tower ID is incorrect"));
		return;
	}
	towerId = inTowerId;
}

int UTowerCsvRow::GetTowerId()
{
	return towerId;
}

void UTowerCsvRow::SetPlaceRound(int inPlaceRound)
{
	if (inPlaceRound < 0) {
		UE_LOG(LogStatisticCollector, Error, TEXT("Round number is incorrect"));
		return;
	}
	placeRound = inPlaceRound;
}

int UTowerCsvRow::GetPlaceRound()
{
	return placeRound;
}

void UTowerCsvRow::SetPlacePoint(FVector inPlacePoint)
{
	placePoint = inPlacePoint;
}

FVector UTowerCsvRow::GetPlacePoint()
{
	return placePoint;
}

void UTowerCsvRow::SetTowerType(FString inTowerType)
{
	towerType = inTowerType;
}

FString UTowerCsvRow::GetTowerType()
{
	return towerType;
}

void UTowerCsvRow::SetHitZone(FString inHitZone)
{
	hitZone = inHitZone;
}

FString UTowerCsvRow::GetHitZone()
{
	return hitZone;
}

void UTowerCsvRow::SetDamageType(FString inDamageType)
{
	damageType = inDamageType;
}

FString UTowerCsvRow::GetDamageType()
{
	return damageType;
}

void UTowerCsvRow::SetDealedDamage(int inDealedDamage)
{
	if (DealedDamage < 0) {
		UE_LOG(LogStatisticCollector, Error, TEXT("Dealed damage is incorrect"));
		return;
	}
	DealedDamage = inDealedDamage;
}

int UTowerCsvRow::GetDealedDamage()
{
	return DealedDamage;
}

FString UTowerCsvRow::GetDataString()
{
	FString outTowerId = FString::FromInt(towerId);
	FString outPlaceRound = FString::FromInt(placeRound);
	FString outPlacePoint = placePoint.ToString();
	FString outDealedDamage = FString::FromInt(DealedDamage);
	return FString(outTowerId + ';' + outPlaceRound + ';' + outPlacePoint + ';' + towerType +
		';' + hitZone + ';' + damageType + ';' + outDealedDamage + '\n');
}

FString UTowerCsvRow::GetHeaderString()
{
	return "Tower ID;Place round;Place point;Tower type;Hit zone;Damage type;Dealed damage\n";
}