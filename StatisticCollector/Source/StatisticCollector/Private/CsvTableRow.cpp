// Fill out your copyright notice in the Description page of Project Settings.


#include "CsvTableRow.h"

UCsvTableRow::UCsvTableRow(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer), rowType(ECsvRowTypes::UnknownRow)
{
}

ECsvRowTypes UCsvTableRow::GetRowType()
{
    return rowType;
}

FString UCsvTableRow::GetDataString()
{
    return FString();
}

FString UCsvTableRow::GetHeaderString()
{
    return FString();
}
