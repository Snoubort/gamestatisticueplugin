// Fill out your copyright notice in the Description page of Project Settings.


#include "CsvTable.h"


UCsvTable::UCsvTable(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer), rowType(ECsvRowTypes::UnknownRow)
{
}

FString UCsvTable::GenerateFileName()
{
	
	FString type = StaticEnum<ECsvRowTypes>()->GetNameStringByValue((int64)rowType);
	FString createTime = FDateTime::Now().ToString();
	return FString(type+'_'+createTime+ ".csv");
}

FString UCsvTable::GetFullPath()
{
	return saveDirectoryPath + '/' + FileName;
}

bool UCsvTable::SaveRowToFile(int rowId)
{
	if (!activRows.IsValidIndex(rowId)) {
		UE_LOG(LogStatisticCollector, Error, TEXT("ID out of range"));
		return false;
	}
	if (FileName.IsEmpty()) {
		FileName = GenerateFileName();
		FFileHelper::SaveStringToFile(activRows[rowId]->GetHeaderString(), *GetFullPath(),
			FFileHelper::EEncodingOptions::AutoDetect, &IFileManager::Get(), EFileWrite::FILEWRITE_Append);
	}
	if (FFileHelper::SaveStringToFile(activRows[rowId]->GetDataString(), *GetFullPath(),
		FFileHelper::EEncodingOptions::AutoDetect, &IFileManager::Get(), EFileWrite::FILEWRITE_Append)) {
		activRows.RemoveAt(rowId);
		return true;
	}
	return false;
}

void UCsvTable::AddRow(UCsvTableRow* newRow)
{
	if (rowType == ECsvRowTypes::UnknownRow) {
		rowType = newRow->GetRowType();
	}
	if (rowType == newRow->GetRowType()) {
		activRows.Add(newRow);
		return;
	}
	UE_LOG(LogStatisticCollector, Error, TEXT("This row is not true type"));
}

TArray<UCsvTableRow*> UCsvTable::GetActivRows()
{
	return activRows;
}

ECsvRowTypes UCsvTable::GetRowType()
{
	return rowType;
}

void UCsvTable::SetSaveDirectory(FString path)
{
	if (!FPaths::DirectoryExists(path)) {
		UE_LOG(LogStatisticCollector, Error, TEXT("This directory dosen't exist"));
		return;
	}
	saveDirectoryPath = path;
}

void UCsvTable::SelectSaveDirectory()
{
	FString path;
	FString windowTitle = "Statistic Tables Directory";

	if (!IsValid(GEngine)) {
		UE_LOG(LogStatisticCollector, Error, TEXT("There is no GEngine"));
		return;
	}
	const void* parentWindow = GEngine->GameViewport->GetWindow()->GetNativeWindow()->GetOSWindowHandle();

	if (FDesktopPlatformModule::Get()->OpenDirectoryDialog(parentWindow, windowTitle, FPaths::AutomationDir(), path)) {
		saveDirectoryPath = path;
		return;
	}
	else {
		UE_LOG(LogStatisticCollector, Error, TEXT("Path doesn't chose"));
		return;
	}
}

FString UCsvTable::GetSaveDirectory()
{
	return saveDirectoryPath;
}
