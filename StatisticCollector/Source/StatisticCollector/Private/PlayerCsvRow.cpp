// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCsvRow.h"

UPlayerCsvRow::UPlayerCsvRow(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	rowType = ECsvRowTypes::PlayerRow;
}

void UPlayerCsvRow::SetId(int id)
{
	if (id < 0) {
		UE_LOG(LogStatisticCollector, Error, TEXT("Player ID can not be a negative number"));
		return;
	}

	playerID = id;
}

int UPlayerCsvRow::GetId()
{
	return playerID;
}

void UPlayerCsvRow::SetRoundNum(int newRoundNumber)
{
	if (newRoundNumber < 0) {
		UE_LOG(LogStatisticCollector, Error, TEXT("Round number can not be a negative number"));
		return;
	}

	roundNum = newRoundNumber;
}

int UPlayerCsvRow::GetRoundNumber()
{
	return roundNum;
}

void UPlayerCsvRow::SetHp(int newHp)
{
	if (newHp < 0) {
		UE_LOG(LogStatisticCollector, Error, TEXT("Hp can not be a negative number"));
		return;
	}

	hp = newHp;
}

int UPlayerCsvRow::GetHp()
{
	return hp;
}

FString UPlayerCsvRow::GetDataString()
{
	FString id = FString::FromInt(playerID);
	FString round = FString::FromInt(roundNum);
	FString healthPoints = FString::FromInt(hp);
	return FString(id + ';' + round + ';' + healthPoints + '\n');
}

FString UPlayerCsvRow::GetHeaderString()
{
	return FString("PlayerID;Round Number;PlayerHP\n");
}
