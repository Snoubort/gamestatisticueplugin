// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "StatisticCollector/Public/ECsvRowTypes.h"

#include "CsvTableRow.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType, Abstract)
class STATISTICCOLLECTOR_API UCsvTableRow : public UObject
{
	GENERATED_BODY()
protected:
	UPROPERTY()
	ECsvRowTypes rowType;

public:
	UCsvTableRow(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable)
	ECsvRowTypes GetRowType();

	UFUNCTION(BlueprintCallable)
	virtual FString GetDataString();

	UFUNCTION(BlueprintCallable)
	virtual FString GetHeaderString();
};
