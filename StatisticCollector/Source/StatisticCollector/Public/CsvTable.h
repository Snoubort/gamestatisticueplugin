// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <DesktopPlatformModule.h>

#include "CsvTableRow.h"
#include "StatisticCollector/Private/Logging.h"

#include "CsvTable.generated.h"

UCLASS(Blueprintable, BlueprintType)
class STATISTICCOLLECTOR_API UCsvTable : public UObject
{
	GENERATED_BODY()

private:
	UPROPERTY()
	TArray<UCsvTableRow*> activRows;

	UPROPERTY()
	ECsvRowTypes rowType;

	UPROPERTY()
	FString saveDirectoryPath;

	UPROPERTY()
	FString FileName;

private:
	FString GenerateFileName();

	FString GetFullPath();

public:
	UCsvTable(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable)
	bool SaveRowToFile(int rowId);

	UFUNCTION(BlueprintCallable)
	void AddRow(UCsvTableRow* newRow);

	UFUNCTION(BlueprintCallable)
	TArray<UCsvTableRow*> GetActivRows();

	UFUNCTION(BlueprintCallable)
	ECsvRowTypes GetRowType();

	UFUNCTION(BlueprintCallable)
	void SetSaveDirectory(FString path);

	UFUNCTION(BlueprintCallable)
	void SelectSaveDirectory();

	UFUNCTION(BlueprintCallable)
	FString GetSaveDirectory();
};
