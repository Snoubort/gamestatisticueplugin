// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Misc/DateTime.h"
#include "Math/Vector.h"

#include "StatisticCollector/Public/CsvTableRow.h"
#include "StatisticCollector/Private/Logging.h"

#include "SpellCsvRow.generated.h"

UCLASS(Blueprintable, BlueprintType)
class STATISTICCOLLECTOR_API USpellCsvRow : public UCsvTableRow {
	GENERATED_BODY()

private:
	UPROPERTY()
	int roundNum;

	UPROPERTY()
	FDateTime usingTime;

	UPROPERTY()
	FVector usingPoint;

	UPROPERTY()
	FString spellType;

public:
	USpellCsvRow(const FObjectInitializer& ObjectInitializer);

	UFUNCTION()
	void SetRoundNum(int round);

	UFUNCTION()
	int GetRoundNum();

	UFUNCTION()
	void SetUsingTime(FDateTime time);

	UFUNCTION()
	FDateTime GetUsingTime();

	UFUNCTION()
	void SetUsingPoint(FVector point);

	UFUNCTION()
	FVector GetUsingPoint();

	UFUNCTION()
	void SetSpellType(FString type);

	UFUNCTION()
	FString GetSpellType();

	virtual FString GetDataString() override;

	virtual FString GetHeaderString() override;
};