// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Misc/DateTime.h"
#include "Math/Vector.h"

#include "StatisticCollector/Public/CsvTableRow.h"
#include "StatisticCollector/Private/Logging.h"

#include "EnemyCsvRow.generated.h"

UCLASS(Blueprintable, BlueprintType)
class STATISTICCOLLECTOR_API UEnemyCsvRow : public UCsvTableRow {
	GENERATED_BODY()
private:
	UPROPERTY()
	int enemyId;

	UPROPERTY()
	int roundNumber;

	UPROPERTY()
	FDateTime spawnTime;

	UPROPERTY()
	FDateTime deathTime;

	UPROPERTY()
	FVector spawnPoint;

	UPROPERTY()
	FVector deathPoint;

	UPROPERTY()
	FString enemyType;
public:
	UEnemyCsvRow(const FObjectInitializer& ObjectInitializer);

	UFUNCTION()
	void SetEnemyId(int inEnemyId);

	UFUNCTION()
	int GetEnemyId();

	UFUNCTION()
	void SetRoundNumber(int inRoundNumber);

	UFUNCTION()
	int GetRoundNumber();

	UFUNCTION()
	void SetSpawnTime(FDateTime inSpawnTime);

	UFUNCTION()
	FDateTime GetSpawnTime();

	UFUNCTION()
	void SetDeathTime(FDateTime inDeathTime);

	UFUNCTION()
	FDateTime GetDeathTime();

	UFUNCTION()
	void SetEnemyType(FString inEnemyType);

	UFUNCTION()
	FString GetEnemyType();

	virtual FString GetDataString() override;

	virtual FString GetHeaderString() override;
};