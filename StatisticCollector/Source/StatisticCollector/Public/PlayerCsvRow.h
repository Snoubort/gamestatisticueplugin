// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "StatisticCollector/Public/CsvTableRow.h"
#include "StatisticCollector/Private/Logging.h"

#include "PlayerCsvRow.generated.h"

UCLASS(Blueprintable, BlueprintType)
class STATISTICCOLLECTOR_API UPlayerCsvRow : public UCsvTableRow
{
	GENERATED_BODY()

private:
	UPROPERTY()
	int playerID;

	UPROPERTY()
	int roundNum;

	UPROPERTY()
	int hp;

public:
	UPlayerCsvRow(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable)
	void SetId(int id);

	UFUNCTION(BlueprintCallable)
	int GetId();

	UFUNCTION(BlueprintCallable)
	void SetRoundNum(int newRoundNumber);

	UFUNCTION(BlueprintCallable)
	int GetRoundNumber();

	UFUNCTION(BlueprintCallable)
	void SetHp(int newHp);

	UFUNCTION(BlueprintCallable)
	int GetHp();

	virtual FString GetDataString() override;

	virtual FString GetHeaderString() override;
};
