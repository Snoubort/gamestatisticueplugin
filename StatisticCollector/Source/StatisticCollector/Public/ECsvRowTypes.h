#pragma once

#include "ECsvRowTypes.generated.h"
UENUM(BlueprintType)
enum class ECsvRowTypes : uint8 {
	UnknownRow	UMETA(DisplayName = "UnknownRow"),
	PlayerRow	UMETA(DisplayName = "PlayerRow"),
	SpellRow	UMETA(DisplayName = "SpellRow"),
};