// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Misc/DateTime.h"
#include "Math/Vector.h"

#include "StatisticCollector/Public/CsvTableRow.h"
#include "StatisticCollector/Private/Logging.h"

#include "TowerCsvRow.generated.h"

UCLASS(Blueprintable, BlueprintType)
class STATISTICCOLLECTOR_API UTowerCsvRow : public UCsvTableRow {
	GENERATED_BODY()
private:
	UPROPERTY()
	int towerId;

	UPROPERTY()
	int placeRound;

	UPROPERTY()
	FVector placePoint;

	UPROPERTY()
	FString towerType;

	UPROPERTY()
	FString hitZone;

	UPROPERTY()
	FString damageType;

	UPROPERTY()
	int DealedDamage;
public:
	UTowerCsvRow(const FObjectInitializer& ObjectInitializer);

	UFUNCTION()
	void SetTowerId(int inTowerId);

	UFUNCTION()
	int GetTowerId();

	UFUNCTION()
	void SetPlaceRound(int inPlaceRound);

	UFUNCTION()
	int GetPlaceRound();

	UFUNCTION()
	void SetPlacePoint(FVector inPlacePoint);

	UFUNCTION()
	FVector GetPlacePoint();

	UFUNCTION()
	void SetTowerType(FString inTowerType);

	UFUNCTION()
	FString GetTowerType();

	UFUNCTION()
	void SetHitZone(FString inHitZone);

	UFUNCTION()
	FString GetHitZone();

	UFUNCTION()
	void SetDamageType(FString inDamageType);

	UFUNCTION()
	FString GetDamageType();

	UFUNCTION()
	void SetDealedDamage(int inDealedDamage);

	UFUNCTION()
	int GetDealedDamage();

	virtual FString GetDataString() override;

	virtual FString GetHeaderString() override;
};