// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "CsvTableRow.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class ECsvRowTypes : uint8;
#ifdef STATISTICCOLLECTOR_CsvTableRow_generated_h
#error "CsvTableRow.generated.h already included, missing '#pragma once' in CsvTableRow.h"
#endif
#define STATISTICCOLLECTOR_CsvTableRow_generated_h

#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_SPARSE_DATA
#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetHeaderString); \
	DECLARE_FUNCTION(execGetDataString); \
	DECLARE_FUNCTION(execGetRowType);


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetHeaderString); \
	DECLARE_FUNCTION(execGetDataString); \
	DECLARE_FUNCTION(execGetRowType);


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_ACCESSORS
#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCsvTableRow(); \
	friend struct Z_Construct_UClass_UCsvTableRow_Statics; \
public: \
	DECLARE_CLASS(UCsvTableRow, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/StatisticCollector"), NO_API) \
	DECLARE_SERIALIZER(UCsvTableRow)


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUCsvTableRow(); \
	friend struct Z_Construct_UClass_UCsvTableRow_Statics; \
public: \
	DECLARE_CLASS(UCsvTableRow, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/StatisticCollector"), NO_API) \
	DECLARE_SERIALIZER(UCsvTableRow)


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCsvTableRow(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCsvTableRow) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCsvTableRow); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCsvTableRow); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCsvTableRow(UCsvTableRow&&); \
	NO_API UCsvTableRow(const UCsvTableRow&); \
public: \
	NO_API virtual ~UCsvTableRow();


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCsvTableRow(UCsvTableRow&&); \
	NO_API UCsvTableRow(const UCsvTableRow&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCsvTableRow); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCsvTableRow); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCsvTableRow) \
	NO_API virtual ~UCsvTableRow();


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_14_PROLOG
#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_SPARSE_DATA \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_RPC_WRAPPERS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_ACCESSORS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_INCLASS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_SPARSE_DATA \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_ACCESSORS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_INCLASS_NO_PURE_DECLS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STATISTICCOLLECTOR_API UClass* StaticClass<class UCsvTableRow>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
