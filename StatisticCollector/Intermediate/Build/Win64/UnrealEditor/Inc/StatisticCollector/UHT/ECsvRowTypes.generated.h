// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ECsvRowTypes.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STATISTICCOLLECTOR_ECsvRowTypes_generated_h
#error "ECsvRowTypes.generated.h already included, missing '#pragma once' in ECsvRowTypes.h"
#endif
#define STATISTICCOLLECTOR_ECsvRowTypes_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_ECsvRowTypes_h


#define FOREACH_ENUM_ECSVROWTYPES(op) \
	op(ECsvRowTypes::UnknownRow) \
	op(ECsvRowTypes::PlayerRow) \
	op(ECsvRowTypes::SpellRow) 

enum class ECsvRowTypes : uint8;
template<> struct TIsUEnumClass<ECsvRowTypes> { enum { Value = true }; };
template<> STATISTICCOLLECTOR_API UEnum* StaticEnum<ECsvRowTypes>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
