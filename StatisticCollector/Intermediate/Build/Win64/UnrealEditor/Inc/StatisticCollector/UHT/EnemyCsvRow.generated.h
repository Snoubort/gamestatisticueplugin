// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "EnemyCsvRow.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDateTime;
#ifdef STATISTICCOLLECTOR_EnemyCsvRow_generated_h
#error "EnemyCsvRow.generated.h already included, missing '#pragma once' in EnemyCsvRow.h"
#endif
#define STATISTICCOLLECTOR_EnemyCsvRow_generated_h

#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_SPARSE_DATA
#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetEnemyType); \
	DECLARE_FUNCTION(execSetEnemyType); \
	DECLARE_FUNCTION(execGetDeathTime); \
	DECLARE_FUNCTION(execSetDeathTime); \
	DECLARE_FUNCTION(execGetSpawnTime); \
	DECLARE_FUNCTION(execSetSpawnTime); \
	DECLARE_FUNCTION(execGetRoundNumber); \
	DECLARE_FUNCTION(execSetRoundNumber); \
	DECLARE_FUNCTION(execGetEnemyId); \
	DECLARE_FUNCTION(execSetEnemyId);


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetEnemyType); \
	DECLARE_FUNCTION(execSetEnemyType); \
	DECLARE_FUNCTION(execGetDeathTime); \
	DECLARE_FUNCTION(execSetDeathTime); \
	DECLARE_FUNCTION(execGetSpawnTime); \
	DECLARE_FUNCTION(execSetSpawnTime); \
	DECLARE_FUNCTION(execGetRoundNumber); \
	DECLARE_FUNCTION(execSetRoundNumber); \
	DECLARE_FUNCTION(execGetEnemyId); \
	DECLARE_FUNCTION(execSetEnemyId);


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_ACCESSORS
#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEnemyCsvRow(); \
	friend struct Z_Construct_UClass_UEnemyCsvRow_Statics; \
public: \
	DECLARE_CLASS(UEnemyCsvRow, UCsvTableRow, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StatisticCollector"), NO_API) \
	DECLARE_SERIALIZER(UEnemyCsvRow)


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUEnemyCsvRow(); \
	friend struct Z_Construct_UClass_UEnemyCsvRow_Statics; \
public: \
	DECLARE_CLASS(UEnemyCsvRow, UCsvTableRow, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StatisticCollector"), NO_API) \
	DECLARE_SERIALIZER(UEnemyCsvRow)


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnemyCsvRow(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnemyCsvRow) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyCsvRow); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyCsvRow); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyCsvRow(UEnemyCsvRow&&); \
	NO_API UEnemyCsvRow(const UEnemyCsvRow&); \
public: \
	NO_API virtual ~UEnemyCsvRow();


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyCsvRow(UEnemyCsvRow&&); \
	NO_API UEnemyCsvRow(const UEnemyCsvRow&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyCsvRow); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyCsvRow); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnemyCsvRow) \
	NO_API virtual ~UEnemyCsvRow();


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_15_PROLOG
#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_SPARSE_DATA \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_RPC_WRAPPERS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_ACCESSORS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_INCLASS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_SPARSE_DATA \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_ACCESSORS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_INCLASS_NO_PURE_DECLS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STATISTICCOLLECTOR_API UClass* StaticClass<class UEnemyCsvRow>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
