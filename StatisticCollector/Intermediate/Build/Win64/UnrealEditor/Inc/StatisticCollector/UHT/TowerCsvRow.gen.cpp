// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StatisticCollector/Public/TowerCsvRow.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTowerCsvRow() {}
// Cross Module References
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_UCsvTableRow();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_UTowerCsvRow();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_UTowerCsvRow_NoRegister();
	UPackage* Z_Construct_UPackage__Script_StatisticCollector();
// End Cross Module References
	DEFINE_FUNCTION(UTowerCsvRow::execGetDealedDamage)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetDealedDamage();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerCsvRow::execSetDealedDamage)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_inDealedDamage);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetDealedDamage(Z_Param_inDealedDamage);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerCsvRow::execGetDamageType)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetDamageType();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerCsvRow::execSetDamageType)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_inDamageType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetDamageType(Z_Param_inDamageType);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerCsvRow::execGetHitZone)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetHitZone();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerCsvRow::execSetHitZone)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_inHitZone);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetHitZone(Z_Param_inHitZone);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerCsvRow::execGetTowerType)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetTowerType();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerCsvRow::execSetTowerType)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_inTowerType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetTowerType(Z_Param_inTowerType);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerCsvRow::execGetPlacePoint)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=P_THIS->GetPlacePoint();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerCsvRow::execSetPlacePoint)
	{
		P_GET_STRUCT(FVector,Z_Param_inPlacePoint);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPlacePoint(Z_Param_inPlacePoint);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerCsvRow::execGetPlaceRound)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetPlaceRound();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerCsvRow::execSetPlaceRound)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_inPlaceRound);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPlaceRound(Z_Param_inPlaceRound);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerCsvRow::execGetTowerId)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetTowerId();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UTowerCsvRow::execSetTowerId)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_inTowerId);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetTowerId(Z_Param_inTowerId);
		P_NATIVE_END;
	}
	void UTowerCsvRow::StaticRegisterNativesUTowerCsvRow()
	{
		UClass* Class = UTowerCsvRow::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDamageType", &UTowerCsvRow::execGetDamageType },
			{ "GetDealedDamage", &UTowerCsvRow::execGetDealedDamage },
			{ "GetHitZone", &UTowerCsvRow::execGetHitZone },
			{ "GetPlacePoint", &UTowerCsvRow::execGetPlacePoint },
			{ "GetPlaceRound", &UTowerCsvRow::execGetPlaceRound },
			{ "GetTowerId", &UTowerCsvRow::execGetTowerId },
			{ "GetTowerType", &UTowerCsvRow::execGetTowerType },
			{ "SetDamageType", &UTowerCsvRow::execSetDamageType },
			{ "SetDealedDamage", &UTowerCsvRow::execSetDealedDamage },
			{ "SetHitZone", &UTowerCsvRow::execSetHitZone },
			{ "SetPlacePoint", &UTowerCsvRow::execSetPlacePoint },
			{ "SetPlaceRound", &UTowerCsvRow::execSetPlaceRound },
			{ "SetTowerId", &UTowerCsvRow::execSetTowerId },
			{ "SetTowerType", &UTowerCsvRow::execSetTowerType },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTowerCsvRow_GetDamageType_Statics
	{
		struct TowerCsvRow_eventGetDamageType_Parms
		{
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTowerCsvRow_GetDamageType_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerCsvRow_eventGetDamageType_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerCsvRow_GetDamageType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerCsvRow_GetDamageType_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerCsvRow_GetDamageType_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerCsvRow_GetDamageType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerCsvRow, nullptr, "GetDamageType", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerCsvRow_GetDamageType_Statics::TowerCsvRow_eventGetDamageType_Parms), Z_Construct_UFunction_UTowerCsvRow_GetDamageType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_GetDamageType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerCsvRow_GetDamageType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_GetDamageType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerCsvRow_GetDamageType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerCsvRow_GetDamageType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerCsvRow_GetDealedDamage_Statics
	{
		struct TowerCsvRow_eventGetDealedDamage_Parms
		{
			int32 ReturnValue;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UTowerCsvRow_GetDealedDamage_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerCsvRow_eventGetDealedDamage_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerCsvRow_GetDealedDamage_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerCsvRow_GetDealedDamage_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerCsvRow_GetDealedDamage_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerCsvRow_GetDealedDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerCsvRow, nullptr, "GetDealedDamage", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerCsvRow_GetDealedDamage_Statics::TowerCsvRow_eventGetDealedDamage_Parms), Z_Construct_UFunction_UTowerCsvRow_GetDealedDamage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_GetDealedDamage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerCsvRow_GetDealedDamage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_GetDealedDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerCsvRow_GetDealedDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerCsvRow_GetDealedDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerCsvRow_GetHitZone_Statics
	{
		struct TowerCsvRow_eventGetHitZone_Parms
		{
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTowerCsvRow_GetHitZone_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerCsvRow_eventGetHitZone_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerCsvRow_GetHitZone_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerCsvRow_GetHitZone_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerCsvRow_GetHitZone_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerCsvRow_GetHitZone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerCsvRow, nullptr, "GetHitZone", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerCsvRow_GetHitZone_Statics::TowerCsvRow_eventGetHitZone_Parms), Z_Construct_UFunction_UTowerCsvRow_GetHitZone_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_GetHitZone_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerCsvRow_GetHitZone_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_GetHitZone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerCsvRow_GetHitZone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerCsvRow_GetHitZone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerCsvRow_GetPlacePoint_Statics
	{
		struct TowerCsvRow_eventGetPlacePoint_Parms
		{
			FVector ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTowerCsvRow_GetPlacePoint_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerCsvRow_eventGetPlacePoint_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerCsvRow_GetPlacePoint_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerCsvRow_GetPlacePoint_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerCsvRow_GetPlacePoint_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerCsvRow_GetPlacePoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerCsvRow, nullptr, "GetPlacePoint", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerCsvRow_GetPlacePoint_Statics::TowerCsvRow_eventGetPlacePoint_Parms), Z_Construct_UFunction_UTowerCsvRow_GetPlacePoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_GetPlacePoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerCsvRow_GetPlacePoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_GetPlacePoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerCsvRow_GetPlacePoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerCsvRow_GetPlacePoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerCsvRow_GetPlaceRound_Statics
	{
		struct TowerCsvRow_eventGetPlaceRound_Parms
		{
			int32 ReturnValue;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UTowerCsvRow_GetPlaceRound_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerCsvRow_eventGetPlaceRound_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerCsvRow_GetPlaceRound_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerCsvRow_GetPlaceRound_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerCsvRow_GetPlaceRound_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerCsvRow_GetPlaceRound_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerCsvRow, nullptr, "GetPlaceRound", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerCsvRow_GetPlaceRound_Statics::TowerCsvRow_eventGetPlaceRound_Parms), Z_Construct_UFunction_UTowerCsvRow_GetPlaceRound_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_GetPlaceRound_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerCsvRow_GetPlaceRound_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_GetPlaceRound_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerCsvRow_GetPlaceRound()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerCsvRow_GetPlaceRound_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerCsvRow_GetTowerId_Statics
	{
		struct TowerCsvRow_eventGetTowerId_Parms
		{
			int32 ReturnValue;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UTowerCsvRow_GetTowerId_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerCsvRow_eventGetTowerId_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerCsvRow_GetTowerId_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerCsvRow_GetTowerId_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerCsvRow_GetTowerId_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerCsvRow_GetTowerId_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerCsvRow, nullptr, "GetTowerId", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerCsvRow_GetTowerId_Statics::TowerCsvRow_eventGetTowerId_Parms), Z_Construct_UFunction_UTowerCsvRow_GetTowerId_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_GetTowerId_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerCsvRow_GetTowerId_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_GetTowerId_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerCsvRow_GetTowerId()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerCsvRow_GetTowerId_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerCsvRow_GetTowerType_Statics
	{
		struct TowerCsvRow_eventGetTowerType_Parms
		{
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTowerCsvRow_GetTowerType_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerCsvRow_eventGetTowerType_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerCsvRow_GetTowerType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerCsvRow_GetTowerType_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerCsvRow_GetTowerType_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerCsvRow_GetTowerType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerCsvRow, nullptr, "GetTowerType", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerCsvRow_GetTowerType_Statics::TowerCsvRow_eventGetTowerType_Parms), Z_Construct_UFunction_UTowerCsvRow_GetTowerType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_GetTowerType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerCsvRow_GetTowerType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_GetTowerType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerCsvRow_GetTowerType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerCsvRow_GetTowerType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerCsvRow_SetDamageType_Statics
	{
		struct TowerCsvRow_eventSetDamageType_Parms
		{
			FString inDamageType;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_inDamageType;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTowerCsvRow_SetDamageType_Statics::NewProp_inDamageType = { "inDamageType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerCsvRow_eventSetDamageType_Parms, inDamageType), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerCsvRow_SetDamageType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerCsvRow_SetDamageType_Statics::NewProp_inDamageType,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerCsvRow_SetDamageType_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerCsvRow_SetDamageType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerCsvRow, nullptr, "SetDamageType", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerCsvRow_SetDamageType_Statics::TowerCsvRow_eventSetDamageType_Parms), Z_Construct_UFunction_UTowerCsvRow_SetDamageType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_SetDamageType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerCsvRow_SetDamageType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_SetDamageType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerCsvRow_SetDamageType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerCsvRow_SetDamageType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerCsvRow_SetDealedDamage_Statics
	{
		struct TowerCsvRow_eventSetDealedDamage_Parms
		{
			int32 inDealedDamage;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_inDealedDamage;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UTowerCsvRow_SetDealedDamage_Statics::NewProp_inDealedDamage = { "inDealedDamage", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerCsvRow_eventSetDealedDamage_Parms, inDealedDamage), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerCsvRow_SetDealedDamage_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerCsvRow_SetDealedDamage_Statics::NewProp_inDealedDamage,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerCsvRow_SetDealedDamage_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerCsvRow_SetDealedDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerCsvRow, nullptr, "SetDealedDamage", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerCsvRow_SetDealedDamage_Statics::TowerCsvRow_eventSetDealedDamage_Parms), Z_Construct_UFunction_UTowerCsvRow_SetDealedDamage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_SetDealedDamage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerCsvRow_SetDealedDamage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_SetDealedDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerCsvRow_SetDealedDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerCsvRow_SetDealedDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerCsvRow_SetHitZone_Statics
	{
		struct TowerCsvRow_eventSetHitZone_Parms
		{
			FString inHitZone;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_inHitZone;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTowerCsvRow_SetHitZone_Statics::NewProp_inHitZone = { "inHitZone", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerCsvRow_eventSetHitZone_Parms, inHitZone), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerCsvRow_SetHitZone_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerCsvRow_SetHitZone_Statics::NewProp_inHitZone,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerCsvRow_SetHitZone_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerCsvRow_SetHitZone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerCsvRow, nullptr, "SetHitZone", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerCsvRow_SetHitZone_Statics::TowerCsvRow_eventSetHitZone_Parms), Z_Construct_UFunction_UTowerCsvRow_SetHitZone_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_SetHitZone_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerCsvRow_SetHitZone_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_SetHitZone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerCsvRow_SetHitZone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerCsvRow_SetHitZone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerCsvRow_SetPlacePoint_Statics
	{
		struct TowerCsvRow_eventSetPlacePoint_Parms
		{
			FVector inPlacePoint;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_inPlacePoint;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTowerCsvRow_SetPlacePoint_Statics::NewProp_inPlacePoint = { "inPlacePoint", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerCsvRow_eventSetPlacePoint_Parms, inPlacePoint), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerCsvRow_SetPlacePoint_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerCsvRow_SetPlacePoint_Statics::NewProp_inPlacePoint,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerCsvRow_SetPlacePoint_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerCsvRow_SetPlacePoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerCsvRow, nullptr, "SetPlacePoint", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerCsvRow_SetPlacePoint_Statics::TowerCsvRow_eventSetPlacePoint_Parms), Z_Construct_UFunction_UTowerCsvRow_SetPlacePoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_SetPlacePoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerCsvRow_SetPlacePoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_SetPlacePoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerCsvRow_SetPlacePoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerCsvRow_SetPlacePoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerCsvRow_SetPlaceRound_Statics
	{
		struct TowerCsvRow_eventSetPlaceRound_Parms
		{
			int32 inPlaceRound;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_inPlaceRound;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UTowerCsvRow_SetPlaceRound_Statics::NewProp_inPlaceRound = { "inPlaceRound", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerCsvRow_eventSetPlaceRound_Parms, inPlaceRound), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerCsvRow_SetPlaceRound_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerCsvRow_SetPlaceRound_Statics::NewProp_inPlaceRound,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerCsvRow_SetPlaceRound_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerCsvRow_SetPlaceRound_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerCsvRow, nullptr, "SetPlaceRound", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerCsvRow_SetPlaceRound_Statics::TowerCsvRow_eventSetPlaceRound_Parms), Z_Construct_UFunction_UTowerCsvRow_SetPlaceRound_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_SetPlaceRound_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerCsvRow_SetPlaceRound_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_SetPlaceRound_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerCsvRow_SetPlaceRound()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerCsvRow_SetPlaceRound_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerCsvRow_SetTowerId_Statics
	{
		struct TowerCsvRow_eventSetTowerId_Parms
		{
			int32 inTowerId;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_inTowerId;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UTowerCsvRow_SetTowerId_Statics::NewProp_inTowerId = { "inTowerId", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerCsvRow_eventSetTowerId_Parms, inTowerId), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerCsvRow_SetTowerId_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerCsvRow_SetTowerId_Statics::NewProp_inTowerId,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerCsvRow_SetTowerId_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerCsvRow_SetTowerId_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerCsvRow, nullptr, "SetTowerId", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerCsvRow_SetTowerId_Statics::TowerCsvRow_eventSetTowerId_Parms), Z_Construct_UFunction_UTowerCsvRow_SetTowerId_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_SetTowerId_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerCsvRow_SetTowerId_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_SetTowerId_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerCsvRow_SetTowerId()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerCsvRow_SetTowerId_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UTowerCsvRow_SetTowerType_Statics
	{
		struct TowerCsvRow_eventSetTowerType_Parms
		{
			FString inTowerType;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_inTowerType;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTowerCsvRow_SetTowerType_Statics::NewProp_inTowerType = { "inTowerType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(TowerCsvRow_eventSetTowerType_Parms, inTowerType), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTowerCsvRow_SetTowerType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTowerCsvRow_SetTowerType_Statics::NewProp_inTowerType,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTowerCsvRow_SetTowerType_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UTowerCsvRow_SetTowerType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTowerCsvRow, nullptr, "SetTowerType", nullptr, nullptr, sizeof(Z_Construct_UFunction_UTowerCsvRow_SetTowerType_Statics::TowerCsvRow_eventSetTowerType_Parms), Z_Construct_UFunction_UTowerCsvRow_SetTowerType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_SetTowerType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTowerCsvRow_SetTowerType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTowerCsvRow_SetTowerType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTowerCsvRow_SetTowerType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UTowerCsvRow_SetTowerType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UTowerCsvRow);
	UClass* Z_Construct_UClass_UTowerCsvRow_NoRegister()
	{
		return UTowerCsvRow::StaticClass();
	}
	struct Z_Construct_UClass_UTowerCsvRow_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_towerId_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_towerId;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_placeRound_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_placeRound;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_placePoint_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_placePoint;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_towerType_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_towerType;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_hitZone_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_hitZone;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_damageType_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_damageType;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DealedDamage_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_DealedDamage;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTowerCsvRow_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCsvTableRow,
		(UObject* (*)())Z_Construct_UPackage__Script_StatisticCollector,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTowerCsvRow_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTowerCsvRow_GetDamageType, "GetDamageType" }, // 2659265487
		{ &Z_Construct_UFunction_UTowerCsvRow_GetDealedDamage, "GetDealedDamage" }, // 1876972383
		{ &Z_Construct_UFunction_UTowerCsvRow_GetHitZone, "GetHitZone" }, // 4203326149
		{ &Z_Construct_UFunction_UTowerCsvRow_GetPlacePoint, "GetPlacePoint" }, // 1367273332
		{ &Z_Construct_UFunction_UTowerCsvRow_GetPlaceRound, "GetPlaceRound" }, // 1259224549
		{ &Z_Construct_UFunction_UTowerCsvRow_GetTowerId, "GetTowerId" }, // 3184212388
		{ &Z_Construct_UFunction_UTowerCsvRow_GetTowerType, "GetTowerType" }, // 44039002
		{ &Z_Construct_UFunction_UTowerCsvRow_SetDamageType, "SetDamageType" }, // 1183263896
		{ &Z_Construct_UFunction_UTowerCsvRow_SetDealedDamage, "SetDealedDamage" }, // 3683277278
		{ &Z_Construct_UFunction_UTowerCsvRow_SetHitZone, "SetHitZone" }, // 2628363780
		{ &Z_Construct_UFunction_UTowerCsvRow_SetPlacePoint, "SetPlacePoint" }, // 3931026762
		{ &Z_Construct_UFunction_UTowerCsvRow_SetPlaceRound, "SetPlaceRound" }, // 1528691146
		{ &Z_Construct_UFunction_UTowerCsvRow_SetTowerId, "SetTowerId" }, // 609568250
		{ &Z_Construct_UFunction_UTowerCsvRow_SetTowerType, "SetTowerType" }, // 1540294754
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerCsvRow_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "TowerCsvRow.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_towerId_MetaData[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_towerId = { "towerId", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerCsvRow, towerId), METADATA_PARAMS(Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_towerId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_towerId_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_placeRound_MetaData[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_placeRound = { "placeRound", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerCsvRow, placeRound), METADATA_PARAMS(Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_placeRound_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_placeRound_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_placePoint_MetaData[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_placePoint = { "placePoint", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerCsvRow, placePoint), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_placePoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_placePoint_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_towerType_MetaData[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_towerType = { "towerType", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerCsvRow, towerType), METADATA_PARAMS(Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_towerType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_towerType_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_hitZone_MetaData[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_hitZone = { "hitZone", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerCsvRow, hitZone), METADATA_PARAMS(Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_hitZone_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_hitZone_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_damageType_MetaData[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_damageType = { "damageType", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerCsvRow, damageType), METADATA_PARAMS(Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_damageType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_damageType_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_DealedDamage_MetaData[] = {
		{ "ModuleRelativePath", "Public/TowerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_DealedDamage = { "DealedDamage", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UTowerCsvRow, DealedDamage), METADATA_PARAMS(Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_DealedDamage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_DealedDamage_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTowerCsvRow_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_towerId,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_placeRound,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_placePoint,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_towerType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_hitZone,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_damageType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTowerCsvRow_Statics::NewProp_DealedDamage,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTowerCsvRow_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTowerCsvRow>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UTowerCsvRow_Statics::ClassParams = {
		&UTowerCsvRow::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UTowerCsvRow_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UTowerCsvRow_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTowerCsvRow_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTowerCsvRow_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTowerCsvRow()
	{
		if (!Z_Registration_Info_UClass_UTowerCsvRow.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UTowerCsvRow.OuterSingleton, Z_Construct_UClass_UTowerCsvRow_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UTowerCsvRow.OuterSingleton;
	}
	template<> STATISTICCOLLECTOR_API UClass* StaticClass<UTowerCsvRow>()
	{
		return UTowerCsvRow::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTowerCsvRow);
	UTowerCsvRow::~UTowerCsvRow() {}
	struct Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UTowerCsvRow, UTowerCsvRow::StaticClass, TEXT("UTowerCsvRow"), &Z_Registration_Info_UClass_UTowerCsvRow, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UTowerCsvRow), 2394551689U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_4051389602(TEXT("/Script/StatisticCollector"),
		Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
