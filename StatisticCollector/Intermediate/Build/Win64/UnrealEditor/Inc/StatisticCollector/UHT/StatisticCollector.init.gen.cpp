// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStatisticCollector_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_StatisticCollector;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_StatisticCollector()
	{
		if (!Z_Registration_Info_UPackage__Script_StatisticCollector.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/StatisticCollector",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0xB868F885,
				0x911C0CF6,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_StatisticCollector.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_StatisticCollector.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_StatisticCollector(Z_Construct_UPackage__Script_StatisticCollector, TEXT("/Script/StatisticCollector"), Z_Registration_Info_UPackage__Script_StatisticCollector, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0xB868F885, 0x911C0CF6));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
