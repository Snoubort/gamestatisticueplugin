// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StatisticCollector/Public/SpellCsvRow.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpellCsvRow() {}
// Cross Module References
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FDateTime();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_UCsvTableRow();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_USpellCsvRow();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_USpellCsvRow_NoRegister();
	UPackage* Z_Construct_UPackage__Script_StatisticCollector();
// End Cross Module References
	DEFINE_FUNCTION(USpellCsvRow::execGetSpellType)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetSpellType();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USpellCsvRow::execSetSpellType)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_type);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSpellType(Z_Param_type);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USpellCsvRow::execGetUsingPoint)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=P_THIS->GetUsingPoint();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USpellCsvRow::execSetUsingPoint)
	{
		P_GET_STRUCT(FVector,Z_Param_point);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetUsingPoint(Z_Param_point);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USpellCsvRow::execGetUsingTime)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FDateTime*)Z_Param__Result=P_THIS->GetUsingTime();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USpellCsvRow::execSetUsingTime)
	{
		P_GET_STRUCT(FDateTime,Z_Param_time);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetUsingTime(Z_Param_time);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USpellCsvRow::execGetRoundNum)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetRoundNum();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USpellCsvRow::execSetRoundNum)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_round);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetRoundNum(Z_Param_round);
		P_NATIVE_END;
	}
	void USpellCsvRow::StaticRegisterNativesUSpellCsvRow()
	{
		UClass* Class = USpellCsvRow::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetRoundNum", &USpellCsvRow::execGetRoundNum },
			{ "GetSpellType", &USpellCsvRow::execGetSpellType },
			{ "GetUsingPoint", &USpellCsvRow::execGetUsingPoint },
			{ "GetUsingTime", &USpellCsvRow::execGetUsingTime },
			{ "SetRoundNum", &USpellCsvRow::execSetRoundNum },
			{ "SetSpellType", &USpellCsvRow::execSetSpellType },
			{ "SetUsingPoint", &USpellCsvRow::execSetUsingPoint },
			{ "SetUsingTime", &USpellCsvRow::execSetUsingTime },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_USpellCsvRow_GetRoundNum_Statics
	{
		struct SpellCsvRow_eventGetRoundNum_Parms
		{
			int32 ReturnValue;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_USpellCsvRow_GetRoundNum_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SpellCsvRow_eventGetRoundNum_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USpellCsvRow_GetRoundNum_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USpellCsvRow_GetRoundNum_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USpellCsvRow_GetRoundNum_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/SpellCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USpellCsvRow_GetRoundNum_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USpellCsvRow, nullptr, "GetRoundNum", nullptr, nullptr, sizeof(Z_Construct_UFunction_USpellCsvRow_GetRoundNum_Statics::SpellCsvRow_eventGetRoundNum_Parms), Z_Construct_UFunction_USpellCsvRow_GetRoundNum_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_GetRoundNum_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USpellCsvRow_GetRoundNum_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_GetRoundNum_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USpellCsvRow_GetRoundNum()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USpellCsvRow_GetRoundNum_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USpellCsvRow_GetSpellType_Statics
	{
		struct SpellCsvRow_eventGetSpellType_Parms
		{
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_USpellCsvRow_GetSpellType_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SpellCsvRow_eventGetSpellType_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USpellCsvRow_GetSpellType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USpellCsvRow_GetSpellType_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USpellCsvRow_GetSpellType_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/SpellCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USpellCsvRow_GetSpellType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USpellCsvRow, nullptr, "GetSpellType", nullptr, nullptr, sizeof(Z_Construct_UFunction_USpellCsvRow_GetSpellType_Statics::SpellCsvRow_eventGetSpellType_Parms), Z_Construct_UFunction_USpellCsvRow_GetSpellType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_GetSpellType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USpellCsvRow_GetSpellType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_GetSpellType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USpellCsvRow_GetSpellType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USpellCsvRow_GetSpellType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USpellCsvRow_GetUsingPoint_Statics
	{
		struct SpellCsvRow_eventGetUsingPoint_Parms
		{
			FVector ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USpellCsvRow_GetUsingPoint_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SpellCsvRow_eventGetUsingPoint_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USpellCsvRow_GetUsingPoint_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USpellCsvRow_GetUsingPoint_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USpellCsvRow_GetUsingPoint_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/SpellCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USpellCsvRow_GetUsingPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USpellCsvRow, nullptr, "GetUsingPoint", nullptr, nullptr, sizeof(Z_Construct_UFunction_USpellCsvRow_GetUsingPoint_Statics::SpellCsvRow_eventGetUsingPoint_Parms), Z_Construct_UFunction_USpellCsvRow_GetUsingPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_GetUsingPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USpellCsvRow_GetUsingPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_GetUsingPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USpellCsvRow_GetUsingPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USpellCsvRow_GetUsingPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USpellCsvRow_GetUsingTime_Statics
	{
		struct SpellCsvRow_eventGetUsingTime_Parms
		{
			FDateTime ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USpellCsvRow_GetUsingTime_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SpellCsvRow_eventGetUsingTime_Parms, ReturnValue), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USpellCsvRow_GetUsingTime_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USpellCsvRow_GetUsingTime_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USpellCsvRow_GetUsingTime_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/SpellCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USpellCsvRow_GetUsingTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USpellCsvRow, nullptr, "GetUsingTime", nullptr, nullptr, sizeof(Z_Construct_UFunction_USpellCsvRow_GetUsingTime_Statics::SpellCsvRow_eventGetUsingTime_Parms), Z_Construct_UFunction_USpellCsvRow_GetUsingTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_GetUsingTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USpellCsvRow_GetUsingTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_GetUsingTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USpellCsvRow_GetUsingTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USpellCsvRow_GetUsingTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USpellCsvRow_SetRoundNum_Statics
	{
		struct SpellCsvRow_eventSetRoundNum_Parms
		{
			int32 round;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_round;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_USpellCsvRow_SetRoundNum_Statics::NewProp_round = { "round", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SpellCsvRow_eventSetRoundNum_Parms, round), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USpellCsvRow_SetRoundNum_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USpellCsvRow_SetRoundNum_Statics::NewProp_round,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USpellCsvRow_SetRoundNum_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/SpellCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USpellCsvRow_SetRoundNum_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USpellCsvRow, nullptr, "SetRoundNum", nullptr, nullptr, sizeof(Z_Construct_UFunction_USpellCsvRow_SetRoundNum_Statics::SpellCsvRow_eventSetRoundNum_Parms), Z_Construct_UFunction_USpellCsvRow_SetRoundNum_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_SetRoundNum_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USpellCsvRow_SetRoundNum_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_SetRoundNum_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USpellCsvRow_SetRoundNum()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USpellCsvRow_SetRoundNum_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USpellCsvRow_SetSpellType_Statics
	{
		struct SpellCsvRow_eventSetSpellType_Parms
		{
			FString type;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_type;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_USpellCsvRow_SetSpellType_Statics::NewProp_type = { "type", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SpellCsvRow_eventSetSpellType_Parms, type), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USpellCsvRow_SetSpellType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USpellCsvRow_SetSpellType_Statics::NewProp_type,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USpellCsvRow_SetSpellType_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/SpellCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USpellCsvRow_SetSpellType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USpellCsvRow, nullptr, "SetSpellType", nullptr, nullptr, sizeof(Z_Construct_UFunction_USpellCsvRow_SetSpellType_Statics::SpellCsvRow_eventSetSpellType_Parms), Z_Construct_UFunction_USpellCsvRow_SetSpellType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_SetSpellType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USpellCsvRow_SetSpellType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_SetSpellType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USpellCsvRow_SetSpellType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USpellCsvRow_SetSpellType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USpellCsvRow_SetUsingPoint_Statics
	{
		struct SpellCsvRow_eventSetUsingPoint_Parms
		{
			FVector point;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_point;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USpellCsvRow_SetUsingPoint_Statics::NewProp_point = { "point", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SpellCsvRow_eventSetUsingPoint_Parms, point), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USpellCsvRow_SetUsingPoint_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USpellCsvRow_SetUsingPoint_Statics::NewProp_point,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USpellCsvRow_SetUsingPoint_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/SpellCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USpellCsvRow_SetUsingPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USpellCsvRow, nullptr, "SetUsingPoint", nullptr, nullptr, sizeof(Z_Construct_UFunction_USpellCsvRow_SetUsingPoint_Statics::SpellCsvRow_eventSetUsingPoint_Parms), Z_Construct_UFunction_USpellCsvRow_SetUsingPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_SetUsingPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USpellCsvRow_SetUsingPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_SetUsingPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USpellCsvRow_SetUsingPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USpellCsvRow_SetUsingPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USpellCsvRow_SetUsingTime_Statics
	{
		struct SpellCsvRow_eventSetUsingTime_Parms
		{
			FDateTime time;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_time;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USpellCsvRow_SetUsingTime_Statics::NewProp_time = { "time", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(SpellCsvRow_eventSetUsingTime_Parms, time), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USpellCsvRow_SetUsingTime_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USpellCsvRow_SetUsingTime_Statics::NewProp_time,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USpellCsvRow_SetUsingTime_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/SpellCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_USpellCsvRow_SetUsingTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USpellCsvRow, nullptr, "SetUsingTime", nullptr, nullptr, sizeof(Z_Construct_UFunction_USpellCsvRow_SetUsingTime_Statics::SpellCsvRow_eventSetUsingTime_Parms), Z_Construct_UFunction_USpellCsvRow_SetUsingTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_SetUsingTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USpellCsvRow_SetUsingTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USpellCsvRow_SetUsingTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USpellCsvRow_SetUsingTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_USpellCsvRow_SetUsingTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(USpellCsvRow);
	UClass* Z_Construct_UClass_USpellCsvRow_NoRegister()
	{
		return USpellCsvRow::StaticClass();
	}
	struct Z_Construct_UClass_USpellCsvRow_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_roundNum_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_roundNum;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_usingTime_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_usingTime;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_usingPoint_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_usingPoint;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_spellType_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_spellType;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USpellCsvRow_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCsvTableRow,
		(UObject* (*)())Z_Construct_UPackage__Script_StatisticCollector,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_USpellCsvRow_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_USpellCsvRow_GetRoundNum, "GetRoundNum" }, // 474312263
		{ &Z_Construct_UFunction_USpellCsvRow_GetSpellType, "GetSpellType" }, // 2822865514
		{ &Z_Construct_UFunction_USpellCsvRow_GetUsingPoint, "GetUsingPoint" }, // 2333443782
		{ &Z_Construct_UFunction_USpellCsvRow_GetUsingTime, "GetUsingTime" }, // 2674603347
		{ &Z_Construct_UFunction_USpellCsvRow_SetRoundNum, "SetRoundNum" }, // 1037669606
		{ &Z_Construct_UFunction_USpellCsvRow_SetSpellType, "SetSpellType" }, // 4226244419
		{ &Z_Construct_UFunction_USpellCsvRow_SetUsingPoint, "SetUsingPoint" }, // 1199236664
		{ &Z_Construct_UFunction_USpellCsvRow_SetUsingTime, "SetUsingTime" }, // 1899665438
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpellCsvRow_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "SpellCsvRow.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/SpellCsvRow.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpellCsvRow_Statics::NewProp_roundNum_MetaData[] = {
		{ "ModuleRelativePath", "Public/SpellCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USpellCsvRow_Statics::NewProp_roundNum = { "roundNum", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(USpellCsvRow, roundNum), METADATA_PARAMS(Z_Construct_UClass_USpellCsvRow_Statics::NewProp_roundNum_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpellCsvRow_Statics::NewProp_roundNum_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpellCsvRow_Statics::NewProp_usingTime_MetaData[] = {
		{ "ModuleRelativePath", "Public/SpellCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_USpellCsvRow_Statics::NewProp_usingTime = { "usingTime", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(USpellCsvRow, usingTime), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(Z_Construct_UClass_USpellCsvRow_Statics::NewProp_usingTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpellCsvRow_Statics::NewProp_usingTime_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpellCsvRow_Statics::NewProp_usingPoint_MetaData[] = {
		{ "ModuleRelativePath", "Public/SpellCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_USpellCsvRow_Statics::NewProp_usingPoint = { "usingPoint", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(USpellCsvRow, usingPoint), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_USpellCsvRow_Statics::NewProp_usingPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpellCsvRow_Statics::NewProp_usingPoint_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpellCsvRow_Statics::NewProp_spellType_MetaData[] = {
		{ "ModuleRelativePath", "Public/SpellCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UClass_USpellCsvRow_Statics::NewProp_spellType = { "spellType", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(USpellCsvRow, spellType), METADATA_PARAMS(Z_Construct_UClass_USpellCsvRow_Statics::NewProp_spellType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpellCsvRow_Statics::NewProp_spellType_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USpellCsvRow_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpellCsvRow_Statics::NewProp_roundNum,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpellCsvRow_Statics::NewProp_usingTime,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpellCsvRow_Statics::NewProp_usingPoint,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpellCsvRow_Statics::NewProp_spellType,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USpellCsvRow_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USpellCsvRow>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_USpellCsvRow_Statics::ClassParams = {
		&USpellCsvRow::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_USpellCsvRow_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_USpellCsvRow_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USpellCsvRow_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USpellCsvRow_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USpellCsvRow()
	{
		if (!Z_Registration_Info_UClass_USpellCsvRow.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_USpellCsvRow.OuterSingleton, Z_Construct_UClass_USpellCsvRow_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_USpellCsvRow.OuterSingleton;
	}
	template<> STATISTICCOLLECTOR_API UClass* StaticClass<USpellCsvRow>()
	{
		return USpellCsvRow::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(USpellCsvRow);
	USpellCsvRow::~USpellCsvRow() {}
	struct Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_USpellCsvRow, USpellCsvRow::StaticClass, TEXT("USpellCsvRow"), &Z_Registration_Info_UClass_USpellCsvRow, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(USpellCsvRow), 2180730320U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_1665194408(TEXT("/Script/StatisticCollector"),
		Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
