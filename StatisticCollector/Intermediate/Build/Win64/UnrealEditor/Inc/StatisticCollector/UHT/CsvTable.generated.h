// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "CsvTable.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UCsvTableRow;
enum class ECsvRowTypes : uint8;
#ifdef STATISTICCOLLECTOR_CsvTable_generated_h
#error "CsvTable.generated.h already included, missing '#pragma once' in CsvTable.h"
#endif
#define STATISTICCOLLECTOR_CsvTable_generated_h

#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_SPARSE_DATA
#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetSaveDirectory); \
	DECLARE_FUNCTION(execSelectSaveDirectory); \
	DECLARE_FUNCTION(execSetSaveDirectory); \
	DECLARE_FUNCTION(execGetRowType); \
	DECLARE_FUNCTION(execGetActivRows); \
	DECLARE_FUNCTION(execAddRow); \
	DECLARE_FUNCTION(execSaveRowToFile);


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetSaveDirectory); \
	DECLARE_FUNCTION(execSelectSaveDirectory); \
	DECLARE_FUNCTION(execSetSaveDirectory); \
	DECLARE_FUNCTION(execGetRowType); \
	DECLARE_FUNCTION(execGetActivRows); \
	DECLARE_FUNCTION(execAddRow); \
	DECLARE_FUNCTION(execSaveRowToFile);


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_ACCESSORS
#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCsvTable(); \
	friend struct Z_Construct_UClass_UCsvTable_Statics; \
public: \
	DECLARE_CLASS(UCsvTable, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StatisticCollector"), NO_API) \
	DECLARE_SERIALIZER(UCsvTable)


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUCsvTable(); \
	friend struct Z_Construct_UClass_UCsvTable_Statics; \
public: \
	DECLARE_CLASS(UCsvTable, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StatisticCollector"), NO_API) \
	DECLARE_SERIALIZER(UCsvTable)


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCsvTable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCsvTable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCsvTable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCsvTable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCsvTable(UCsvTable&&); \
	NO_API UCsvTable(const UCsvTable&); \
public: \
	NO_API virtual ~UCsvTable();


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCsvTable(UCsvTable&&); \
	NO_API UCsvTable(const UCsvTable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCsvTable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCsvTable); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCsvTable) \
	NO_API virtual ~UCsvTable();


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_13_PROLOG
#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_SPARSE_DATA \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_RPC_WRAPPERS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_ACCESSORS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_INCLASS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_SPARSE_DATA \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_ACCESSORS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_INCLASS_NO_PURE_DECLS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STATISTICCOLLECTOR_API UClass* StaticClass<class UCsvTable>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
