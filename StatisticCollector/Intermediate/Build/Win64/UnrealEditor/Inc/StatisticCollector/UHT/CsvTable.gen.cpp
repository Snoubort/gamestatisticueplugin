// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StatisticCollector/Public/CsvTable.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCsvTable() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_UCsvTable();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_UCsvTable_NoRegister();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_UCsvTableRow_NoRegister();
	STATISTICCOLLECTOR_API UEnum* Z_Construct_UEnum_StatisticCollector_ECsvRowTypes();
	UPackage* Z_Construct_UPackage__Script_StatisticCollector();
// End Cross Module References
	DEFINE_FUNCTION(UCsvTable::execGetSaveDirectory)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetSaveDirectory();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCsvTable::execSelectSaveDirectory)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SelectSaveDirectory();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCsvTable::execSetSaveDirectory)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_path);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSaveDirectory(Z_Param_path);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCsvTable::execGetRowType)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ECsvRowTypes*)Z_Param__Result=P_THIS->GetRowType();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCsvTable::execGetActivRows)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UCsvTableRow*>*)Z_Param__Result=P_THIS->GetActivRows();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCsvTable::execAddRow)
	{
		P_GET_OBJECT(UCsvTableRow,Z_Param_newRow);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddRow(Z_Param_newRow);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCsvTable::execSaveRowToFile)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_rowId);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SaveRowToFile(Z_Param_rowId);
		P_NATIVE_END;
	}
	void UCsvTable::StaticRegisterNativesUCsvTable()
	{
		UClass* Class = UCsvTable::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddRow", &UCsvTable::execAddRow },
			{ "GetActivRows", &UCsvTable::execGetActivRows },
			{ "GetRowType", &UCsvTable::execGetRowType },
			{ "GetSaveDirectory", &UCsvTable::execGetSaveDirectory },
			{ "SaveRowToFile", &UCsvTable::execSaveRowToFile },
			{ "SelectSaveDirectory", &UCsvTable::execSelectSaveDirectory },
			{ "SetSaveDirectory", &UCsvTable::execSetSaveDirectory },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCsvTable_AddRow_Statics
	{
		struct CsvTable_eventAddRow_Parms
		{
			UCsvTableRow* newRow;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_newRow;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCsvTable_AddRow_Statics::NewProp_newRow = { "newRow", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CsvTable_eventAddRow_Parms, newRow), Z_Construct_UClass_UCsvTableRow_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCsvTable_AddRow_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCsvTable_AddRow_Statics::NewProp_newRow,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCsvTable_AddRow_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CsvTable.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCsvTable_AddRow_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCsvTable, nullptr, "AddRow", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCsvTable_AddRow_Statics::CsvTable_eventAddRow_Parms), Z_Construct_UFunction_UCsvTable_AddRow_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTable_AddRow_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCsvTable_AddRow_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTable_AddRow_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCsvTable_AddRow()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCsvTable_AddRow_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCsvTable_GetActivRows_Statics
	{
		struct CsvTable_eventGetActivRows_Parms
		{
			TArray<UCsvTableRow*> ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCsvTable_GetActivRows_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_UCsvTableRow_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UCsvTable_GetActivRows_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CsvTable_eventGetActivRows_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCsvTable_GetActivRows_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCsvTable_GetActivRows_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCsvTable_GetActivRows_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCsvTable_GetActivRows_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CsvTable.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCsvTable_GetActivRows_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCsvTable, nullptr, "GetActivRows", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCsvTable_GetActivRows_Statics::CsvTable_eventGetActivRows_Parms), Z_Construct_UFunction_UCsvTable_GetActivRows_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTable_GetActivRows_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCsvTable_GetActivRows_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTable_GetActivRows_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCsvTable_GetActivRows()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCsvTable_GetActivRows_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCsvTable_GetRowType_Statics
	{
		struct CsvTable_eventGetRowType_Parms
		{
			ECsvRowTypes ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UCsvTable_GetRowType_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UCsvTable_GetRowType_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CsvTable_eventGetRowType_Parms, ReturnValue), Z_Construct_UEnum_StatisticCollector_ECsvRowTypes, METADATA_PARAMS(nullptr, 0) }; // 1594374167
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCsvTable_GetRowType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCsvTable_GetRowType_Statics::NewProp_ReturnValue_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCsvTable_GetRowType_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCsvTable_GetRowType_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CsvTable.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCsvTable_GetRowType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCsvTable, nullptr, "GetRowType", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCsvTable_GetRowType_Statics::CsvTable_eventGetRowType_Parms), Z_Construct_UFunction_UCsvTable_GetRowType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTable_GetRowType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCsvTable_GetRowType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTable_GetRowType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCsvTable_GetRowType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCsvTable_GetRowType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCsvTable_GetSaveDirectory_Statics
	{
		struct CsvTable_eventGetSaveDirectory_Parms
		{
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCsvTable_GetSaveDirectory_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CsvTable_eventGetSaveDirectory_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCsvTable_GetSaveDirectory_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCsvTable_GetSaveDirectory_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCsvTable_GetSaveDirectory_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CsvTable.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCsvTable_GetSaveDirectory_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCsvTable, nullptr, "GetSaveDirectory", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCsvTable_GetSaveDirectory_Statics::CsvTable_eventGetSaveDirectory_Parms), Z_Construct_UFunction_UCsvTable_GetSaveDirectory_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTable_GetSaveDirectory_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCsvTable_GetSaveDirectory_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTable_GetSaveDirectory_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCsvTable_GetSaveDirectory()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCsvTable_GetSaveDirectory_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics
	{
		struct CsvTable_eventSaveRowToFile_Parms
		{
			int32 rowId;
			bool ReturnValue;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_rowId;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics::NewProp_rowId = { "rowId", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CsvTable_eventSaveRowToFile_Parms, rowId), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CsvTable_eventSaveRowToFile_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(CsvTable_eventSaveRowToFile_Parms), &Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics::NewProp_rowId,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CsvTable.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCsvTable, nullptr, "SaveRowToFile", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics::CsvTable_eventSaveRowToFile_Parms), Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCsvTable_SaveRowToFile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCsvTable_SaveRowToFile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCsvTable_SelectSaveDirectory_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCsvTable_SelectSaveDirectory_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CsvTable.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCsvTable_SelectSaveDirectory_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCsvTable, nullptr, "SelectSaveDirectory", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCsvTable_SelectSaveDirectory_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTable_SelectSaveDirectory_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCsvTable_SelectSaveDirectory()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCsvTable_SelectSaveDirectory_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCsvTable_SetSaveDirectory_Statics
	{
		struct CsvTable_eventSetSaveDirectory_Parms
		{
			FString path;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_path;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCsvTable_SetSaveDirectory_Statics::NewProp_path = { "path", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CsvTable_eventSetSaveDirectory_Parms, path), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCsvTable_SetSaveDirectory_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCsvTable_SetSaveDirectory_Statics::NewProp_path,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCsvTable_SetSaveDirectory_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CsvTable.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCsvTable_SetSaveDirectory_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCsvTable, nullptr, "SetSaveDirectory", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCsvTable_SetSaveDirectory_Statics::CsvTable_eventSetSaveDirectory_Parms), Z_Construct_UFunction_UCsvTable_SetSaveDirectory_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTable_SetSaveDirectory_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCsvTable_SetSaveDirectory_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTable_SetSaveDirectory_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCsvTable_SetSaveDirectory()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCsvTable_SetSaveDirectory_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCsvTable);
	UClass* Z_Construct_UClass_UCsvTable_NoRegister()
	{
		return UCsvTable::StaticClass();
	}
	struct Z_Construct_UClass_UCsvTable_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_activRows_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_activRows_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_activRows;
		static const UECodeGen_Private::FBytePropertyParams NewProp_rowType_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_rowType_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_rowType;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_saveDirectoryPath_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_saveDirectoryPath;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FileName_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_FileName;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCsvTable_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_StatisticCollector,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCsvTable_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCsvTable_AddRow, "AddRow" }, // 2891864700
		{ &Z_Construct_UFunction_UCsvTable_GetActivRows, "GetActivRows" }, // 4271035775
		{ &Z_Construct_UFunction_UCsvTable_GetRowType, "GetRowType" }, // 3333279749
		{ &Z_Construct_UFunction_UCsvTable_GetSaveDirectory, "GetSaveDirectory" }, // 4211855879
		{ &Z_Construct_UFunction_UCsvTable_SaveRowToFile, "SaveRowToFile" }, // 3595562182
		{ &Z_Construct_UFunction_UCsvTable_SelectSaveDirectory, "SelectSaveDirectory" }, // 62570831
		{ &Z_Construct_UFunction_UCsvTable_SetSaveDirectory, "SetSaveDirectory" }, // 1237426550
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCsvTable_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "CsvTable.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/CsvTable.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCsvTable_Statics::NewProp_activRows_Inner = { "activRows", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UClass_UCsvTableRow_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCsvTable_Statics::NewProp_activRows_MetaData[] = {
		{ "ModuleRelativePath", "Public/CsvTable.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCsvTable_Statics::NewProp_activRows = { "activRows", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCsvTable, activRows), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCsvTable_Statics::NewProp_activRows_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCsvTable_Statics::NewProp_activRows_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCsvTable_Statics::NewProp_rowType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCsvTable_Statics::NewProp_rowType_MetaData[] = {
		{ "ModuleRelativePath", "Public/CsvTable.h" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCsvTable_Statics::NewProp_rowType = { "rowType", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCsvTable, rowType), Z_Construct_UEnum_StatisticCollector_ECsvRowTypes, METADATA_PARAMS(Z_Construct_UClass_UCsvTable_Statics::NewProp_rowType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCsvTable_Statics::NewProp_rowType_MetaData)) }; // 1594374167
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCsvTable_Statics::NewProp_saveDirectoryPath_MetaData[] = {
		{ "ModuleRelativePath", "Public/CsvTable.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UClass_UCsvTable_Statics::NewProp_saveDirectoryPath = { "saveDirectoryPath", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCsvTable, saveDirectoryPath), METADATA_PARAMS(Z_Construct_UClass_UCsvTable_Statics::NewProp_saveDirectoryPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCsvTable_Statics::NewProp_saveDirectoryPath_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCsvTable_Statics::NewProp_FileName_MetaData[] = {
		{ "ModuleRelativePath", "Public/CsvTable.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UClass_UCsvTable_Statics::NewProp_FileName = { "FileName", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCsvTable, FileName), METADATA_PARAMS(Z_Construct_UClass_UCsvTable_Statics::NewProp_FileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCsvTable_Statics::NewProp_FileName_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCsvTable_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCsvTable_Statics::NewProp_activRows_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCsvTable_Statics::NewProp_activRows,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCsvTable_Statics::NewProp_rowType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCsvTable_Statics::NewProp_rowType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCsvTable_Statics::NewProp_saveDirectoryPath,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCsvTable_Statics::NewProp_FileName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCsvTable_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCsvTable>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCsvTable_Statics::ClassParams = {
		&UCsvTable::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCsvTable_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCsvTable_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCsvTable_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCsvTable_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCsvTable()
	{
		if (!Z_Registration_Info_UClass_UCsvTable.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCsvTable.OuterSingleton, Z_Construct_UClass_UCsvTable_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCsvTable.OuterSingleton;
	}
	template<> STATISTICCOLLECTOR_API UClass* StaticClass<UCsvTable>()
	{
		return UCsvTable::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCsvTable);
	UCsvTable::~UCsvTable() {}
	struct Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCsvTable, UCsvTable::StaticClass, TEXT("UCsvTable"), &Z_Registration_Info_UClass_UCsvTable, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCsvTable), 15166313U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_1081862565(TEXT("/Script/StatisticCollector"),
		Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTable_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
