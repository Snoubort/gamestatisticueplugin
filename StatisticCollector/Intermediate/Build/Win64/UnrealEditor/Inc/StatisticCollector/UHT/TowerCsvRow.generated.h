// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "TowerCsvRow.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STATISTICCOLLECTOR_TowerCsvRow_generated_h
#error "TowerCsvRow.generated.h already included, missing '#pragma once' in TowerCsvRow.h"
#endif
#define STATISTICCOLLECTOR_TowerCsvRow_generated_h

#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_SPARSE_DATA
#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDealedDamage); \
	DECLARE_FUNCTION(execSetDealedDamage); \
	DECLARE_FUNCTION(execGetDamageType); \
	DECLARE_FUNCTION(execSetDamageType); \
	DECLARE_FUNCTION(execGetHitZone); \
	DECLARE_FUNCTION(execSetHitZone); \
	DECLARE_FUNCTION(execGetTowerType); \
	DECLARE_FUNCTION(execSetTowerType); \
	DECLARE_FUNCTION(execGetPlacePoint); \
	DECLARE_FUNCTION(execSetPlacePoint); \
	DECLARE_FUNCTION(execGetPlaceRound); \
	DECLARE_FUNCTION(execSetPlaceRound); \
	DECLARE_FUNCTION(execGetTowerId); \
	DECLARE_FUNCTION(execSetTowerId);


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDealedDamage); \
	DECLARE_FUNCTION(execSetDealedDamage); \
	DECLARE_FUNCTION(execGetDamageType); \
	DECLARE_FUNCTION(execSetDamageType); \
	DECLARE_FUNCTION(execGetHitZone); \
	DECLARE_FUNCTION(execSetHitZone); \
	DECLARE_FUNCTION(execGetTowerType); \
	DECLARE_FUNCTION(execSetTowerType); \
	DECLARE_FUNCTION(execGetPlacePoint); \
	DECLARE_FUNCTION(execSetPlacePoint); \
	DECLARE_FUNCTION(execGetPlaceRound); \
	DECLARE_FUNCTION(execSetPlaceRound); \
	DECLARE_FUNCTION(execGetTowerId); \
	DECLARE_FUNCTION(execSetTowerId);


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_ACCESSORS
#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTowerCsvRow(); \
	friend struct Z_Construct_UClass_UTowerCsvRow_Statics; \
public: \
	DECLARE_CLASS(UTowerCsvRow, UCsvTableRow, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StatisticCollector"), NO_API) \
	DECLARE_SERIALIZER(UTowerCsvRow)


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUTowerCsvRow(); \
	friend struct Z_Construct_UClass_UTowerCsvRow_Statics; \
public: \
	DECLARE_CLASS(UTowerCsvRow, UCsvTableRow, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StatisticCollector"), NO_API) \
	DECLARE_SERIALIZER(UTowerCsvRow)


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTowerCsvRow(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTowerCsvRow) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTowerCsvRow); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTowerCsvRow); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTowerCsvRow(UTowerCsvRow&&); \
	NO_API UTowerCsvRow(const UTowerCsvRow&); \
public: \
	NO_API virtual ~UTowerCsvRow();


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTowerCsvRow(UTowerCsvRow&&); \
	NO_API UTowerCsvRow(const UTowerCsvRow&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTowerCsvRow); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTowerCsvRow); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTowerCsvRow) \
	NO_API virtual ~UTowerCsvRow();


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_15_PROLOG
#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_SPARSE_DATA \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_RPC_WRAPPERS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_ACCESSORS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_INCLASS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_SPARSE_DATA \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_ACCESSORS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_INCLASS_NO_PURE_DECLS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STATISTICCOLLECTOR_API UClass* StaticClass<class UTowerCsvRow>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_TowerCsvRow_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
