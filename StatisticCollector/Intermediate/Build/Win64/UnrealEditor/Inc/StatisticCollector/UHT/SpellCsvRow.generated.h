// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "SpellCsvRow.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDateTime;
#ifdef STATISTICCOLLECTOR_SpellCsvRow_generated_h
#error "SpellCsvRow.generated.h already included, missing '#pragma once' in SpellCsvRow.h"
#endif
#define STATISTICCOLLECTOR_SpellCsvRow_generated_h

#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_SPARSE_DATA
#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetSpellType); \
	DECLARE_FUNCTION(execSetSpellType); \
	DECLARE_FUNCTION(execGetUsingPoint); \
	DECLARE_FUNCTION(execSetUsingPoint); \
	DECLARE_FUNCTION(execGetUsingTime); \
	DECLARE_FUNCTION(execSetUsingTime); \
	DECLARE_FUNCTION(execGetRoundNum); \
	DECLARE_FUNCTION(execSetRoundNum);


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetSpellType); \
	DECLARE_FUNCTION(execSetSpellType); \
	DECLARE_FUNCTION(execGetUsingPoint); \
	DECLARE_FUNCTION(execSetUsingPoint); \
	DECLARE_FUNCTION(execGetUsingTime); \
	DECLARE_FUNCTION(execSetUsingTime); \
	DECLARE_FUNCTION(execGetRoundNum); \
	DECLARE_FUNCTION(execSetRoundNum);


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_ACCESSORS
#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSpellCsvRow(); \
	friend struct Z_Construct_UClass_USpellCsvRow_Statics; \
public: \
	DECLARE_CLASS(USpellCsvRow, UCsvTableRow, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StatisticCollector"), NO_API) \
	DECLARE_SERIALIZER(USpellCsvRow)


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUSpellCsvRow(); \
	friend struct Z_Construct_UClass_USpellCsvRow_Statics; \
public: \
	DECLARE_CLASS(USpellCsvRow, UCsvTableRow, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/StatisticCollector"), NO_API) \
	DECLARE_SERIALIZER(USpellCsvRow)


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpellCsvRow(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpellCsvRow) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpellCsvRow); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpellCsvRow); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpellCsvRow(USpellCsvRow&&); \
	NO_API USpellCsvRow(const USpellCsvRow&); \
public: \
	NO_API virtual ~USpellCsvRow();


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpellCsvRow(USpellCsvRow&&); \
	NO_API USpellCsvRow(const USpellCsvRow&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpellCsvRow); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpellCsvRow); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpellCsvRow) \
	NO_API virtual ~USpellCsvRow();


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_15_PROLOG
#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_SPARSE_DATA \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_RPC_WRAPPERS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_ACCESSORS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_INCLASS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_SPARSE_DATA \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_ACCESSORS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_INCLASS_NO_PURE_DECLS \
	FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STATISTICCOLLECTOR_API UClass* StaticClass<class USpellCsvRow>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_SpellCsvRow_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
