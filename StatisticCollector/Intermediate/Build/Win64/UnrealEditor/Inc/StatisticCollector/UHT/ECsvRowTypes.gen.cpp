// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StatisticCollector/Public/ECsvRowTypes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeECsvRowTypes() {}
// Cross Module References
	STATISTICCOLLECTOR_API UEnum* Z_Construct_UEnum_StatisticCollector_ECsvRowTypes();
	UPackage* Z_Construct_UPackage__Script_StatisticCollector();
// End Cross Module References
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_ECsvRowTypes;
	static UEnum* ECsvRowTypes_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_ECsvRowTypes.OuterSingleton)
		{
			Z_Registration_Info_UEnum_ECsvRowTypes.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_StatisticCollector_ECsvRowTypes, (UObject*)Z_Construct_UPackage__Script_StatisticCollector(), TEXT("ECsvRowTypes"));
		}
		return Z_Registration_Info_UEnum_ECsvRowTypes.OuterSingleton;
	}
	template<> STATISTICCOLLECTOR_API UEnum* StaticEnum<ECsvRowTypes>()
	{
		return ECsvRowTypes_StaticEnum();
	}
	struct Z_Construct_UEnum_StatisticCollector_ECsvRowTypes_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_StatisticCollector_ECsvRowTypes_Statics::Enumerators[] = {
		{ "ECsvRowTypes::UnknownRow", (int64)ECsvRowTypes::UnknownRow },
		{ "ECsvRowTypes::PlayerRow", (int64)ECsvRowTypes::PlayerRow },
		{ "ECsvRowTypes::SpellRow", (int64)ECsvRowTypes::SpellRow },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_StatisticCollector_ECsvRowTypes_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/ECsvRowTypes.h" },
		{ "PlayerRow.DisplayName", "PlayerRow" },
		{ "PlayerRow.Name", "ECsvRowTypes::PlayerRow" },
		{ "SpellRow.DisplayName", "SpellRow" },
		{ "SpellRow.Name", "ECsvRowTypes::SpellRow" },
		{ "UnknownRow.DisplayName", "UnknownRow" },
		{ "UnknownRow.Name", "ECsvRowTypes::UnknownRow" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_StatisticCollector_ECsvRowTypes_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_StatisticCollector,
		nullptr,
		"ECsvRowTypes",
		"ECsvRowTypes",
		Z_Construct_UEnum_StatisticCollector_ECsvRowTypes_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_StatisticCollector_ECsvRowTypes_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_StatisticCollector_ECsvRowTypes_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_StatisticCollector_ECsvRowTypes_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_StatisticCollector_ECsvRowTypes()
	{
		if (!Z_Registration_Info_UEnum_ECsvRowTypes.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_ECsvRowTypes.InnerSingleton, Z_Construct_UEnum_StatisticCollector_ECsvRowTypes_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_ECsvRowTypes.InnerSingleton;
	}
	struct Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_ECsvRowTypes_h_Statics
	{
		static const FEnumRegisterCompiledInInfo EnumInfo[];
	};
	const FEnumRegisterCompiledInInfo Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_ECsvRowTypes_h_Statics::EnumInfo[] = {
		{ ECsvRowTypes_StaticEnum, TEXT("ECsvRowTypes"), &Z_Registration_Info_UEnum_ECsvRowTypes, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 1594374167U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_ECsvRowTypes_h_171564966(TEXT("/Script/StatisticCollector"),
		nullptr, 0,
		nullptr, 0,
		Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_ECsvRowTypes_h_Statics::EnumInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_ECsvRowTypes_h_Statics::EnumInfo));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
