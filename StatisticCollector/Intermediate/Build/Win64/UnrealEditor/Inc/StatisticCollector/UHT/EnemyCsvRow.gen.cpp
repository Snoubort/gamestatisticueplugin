// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StatisticCollector/Public/EnemyCsvRow.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEnemyCsvRow() {}
// Cross Module References
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FDateTime();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_UCsvTableRow();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_UEnemyCsvRow();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_UEnemyCsvRow_NoRegister();
	UPackage* Z_Construct_UPackage__Script_StatisticCollector();
// End Cross Module References
	DEFINE_FUNCTION(UEnemyCsvRow::execGetEnemyType)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetEnemyType();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEnemyCsvRow::execSetEnemyType)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_inEnemyType);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetEnemyType(Z_Param_inEnemyType);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEnemyCsvRow::execGetDeathTime)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FDateTime*)Z_Param__Result=P_THIS->GetDeathTime();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEnemyCsvRow::execSetDeathTime)
	{
		P_GET_STRUCT(FDateTime,Z_Param_inDeathTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetDeathTime(Z_Param_inDeathTime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEnemyCsvRow::execGetSpawnTime)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FDateTime*)Z_Param__Result=P_THIS->GetSpawnTime();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEnemyCsvRow::execSetSpawnTime)
	{
		P_GET_STRUCT(FDateTime,Z_Param_inSpawnTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSpawnTime(Z_Param_inSpawnTime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEnemyCsvRow::execGetRoundNumber)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetRoundNumber();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEnemyCsvRow::execSetRoundNumber)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_inRoundNumber);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetRoundNumber(Z_Param_inRoundNumber);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEnemyCsvRow::execGetEnemyId)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetEnemyId();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UEnemyCsvRow::execSetEnemyId)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_inEnemyId);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetEnemyId(Z_Param_inEnemyId);
		P_NATIVE_END;
	}
	void UEnemyCsvRow::StaticRegisterNativesUEnemyCsvRow()
	{
		UClass* Class = UEnemyCsvRow::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDeathTime", &UEnemyCsvRow::execGetDeathTime },
			{ "GetEnemyId", &UEnemyCsvRow::execGetEnemyId },
			{ "GetEnemyType", &UEnemyCsvRow::execGetEnemyType },
			{ "GetRoundNumber", &UEnemyCsvRow::execGetRoundNumber },
			{ "GetSpawnTime", &UEnemyCsvRow::execGetSpawnTime },
			{ "SetDeathTime", &UEnemyCsvRow::execSetDeathTime },
			{ "SetEnemyId", &UEnemyCsvRow::execSetEnemyId },
			{ "SetEnemyType", &UEnemyCsvRow::execSetEnemyType },
			{ "SetRoundNumber", &UEnemyCsvRow::execSetRoundNumber },
			{ "SetSpawnTime", &UEnemyCsvRow::execSetSpawnTime },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEnemyCsvRow_GetDeathTime_Statics
	{
		struct EnemyCsvRow_eventGetDeathTime_Parms
		{
			FDateTime ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEnemyCsvRow_GetDeathTime_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(EnemyCsvRow_eventGetDeathTime_Parms, ReturnValue), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnemyCsvRow_GetDeathTime_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnemyCsvRow_GetDeathTime_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnemyCsvRow_GetDeathTime_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnemyCsvRow_GetDeathTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnemyCsvRow, nullptr, "GetDeathTime", nullptr, nullptr, sizeof(Z_Construct_UFunction_UEnemyCsvRow_GetDeathTime_Statics::EnemyCsvRow_eventGetDeathTime_Parms), Z_Construct_UFunction_UEnemyCsvRow_GetDeathTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_GetDeathTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnemyCsvRow_GetDeathTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_GetDeathTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnemyCsvRow_GetDeathTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UEnemyCsvRow_GetDeathTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnemyCsvRow_GetEnemyId_Statics
	{
		struct EnemyCsvRow_eventGetEnemyId_Parms
		{
			int32 ReturnValue;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UEnemyCsvRow_GetEnemyId_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(EnemyCsvRow_eventGetEnemyId_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnemyCsvRow_GetEnemyId_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnemyCsvRow_GetEnemyId_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnemyCsvRow_GetEnemyId_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnemyCsvRow_GetEnemyId_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnemyCsvRow, nullptr, "GetEnemyId", nullptr, nullptr, sizeof(Z_Construct_UFunction_UEnemyCsvRow_GetEnemyId_Statics::EnemyCsvRow_eventGetEnemyId_Parms), Z_Construct_UFunction_UEnemyCsvRow_GetEnemyId_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_GetEnemyId_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnemyCsvRow_GetEnemyId_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_GetEnemyId_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnemyCsvRow_GetEnemyId()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UEnemyCsvRow_GetEnemyId_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnemyCsvRow_GetEnemyType_Statics
	{
		struct EnemyCsvRow_eventGetEnemyType_Parms
		{
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEnemyCsvRow_GetEnemyType_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(EnemyCsvRow_eventGetEnemyType_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnemyCsvRow_GetEnemyType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnemyCsvRow_GetEnemyType_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnemyCsvRow_GetEnemyType_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnemyCsvRow_GetEnemyType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnemyCsvRow, nullptr, "GetEnemyType", nullptr, nullptr, sizeof(Z_Construct_UFunction_UEnemyCsvRow_GetEnemyType_Statics::EnemyCsvRow_eventGetEnemyType_Parms), Z_Construct_UFunction_UEnemyCsvRow_GetEnemyType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_GetEnemyType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnemyCsvRow_GetEnemyType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_GetEnemyType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnemyCsvRow_GetEnemyType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UEnemyCsvRow_GetEnemyType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnemyCsvRow_GetRoundNumber_Statics
	{
		struct EnemyCsvRow_eventGetRoundNumber_Parms
		{
			int32 ReturnValue;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UEnemyCsvRow_GetRoundNumber_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(EnemyCsvRow_eventGetRoundNumber_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnemyCsvRow_GetRoundNumber_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnemyCsvRow_GetRoundNumber_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnemyCsvRow_GetRoundNumber_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnemyCsvRow_GetRoundNumber_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnemyCsvRow, nullptr, "GetRoundNumber", nullptr, nullptr, sizeof(Z_Construct_UFunction_UEnemyCsvRow_GetRoundNumber_Statics::EnemyCsvRow_eventGetRoundNumber_Parms), Z_Construct_UFunction_UEnemyCsvRow_GetRoundNumber_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_GetRoundNumber_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnemyCsvRow_GetRoundNumber_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_GetRoundNumber_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnemyCsvRow_GetRoundNumber()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UEnemyCsvRow_GetRoundNumber_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnemyCsvRow_GetSpawnTime_Statics
	{
		struct EnemyCsvRow_eventGetSpawnTime_Parms
		{
			FDateTime ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEnemyCsvRow_GetSpawnTime_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(EnemyCsvRow_eventGetSpawnTime_Parms, ReturnValue), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnemyCsvRow_GetSpawnTime_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnemyCsvRow_GetSpawnTime_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnemyCsvRow_GetSpawnTime_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnemyCsvRow_GetSpawnTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnemyCsvRow, nullptr, "GetSpawnTime", nullptr, nullptr, sizeof(Z_Construct_UFunction_UEnemyCsvRow_GetSpawnTime_Statics::EnemyCsvRow_eventGetSpawnTime_Parms), Z_Construct_UFunction_UEnemyCsvRow_GetSpawnTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_GetSpawnTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnemyCsvRow_GetSpawnTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_GetSpawnTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnemyCsvRow_GetSpawnTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UEnemyCsvRow_GetSpawnTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnemyCsvRow_SetDeathTime_Statics
	{
		struct EnemyCsvRow_eventSetDeathTime_Parms
		{
			FDateTime inDeathTime;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_inDeathTime;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEnemyCsvRow_SetDeathTime_Statics::NewProp_inDeathTime = { "inDeathTime", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(EnemyCsvRow_eventSetDeathTime_Parms, inDeathTime), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnemyCsvRow_SetDeathTime_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnemyCsvRow_SetDeathTime_Statics::NewProp_inDeathTime,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnemyCsvRow_SetDeathTime_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnemyCsvRow_SetDeathTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnemyCsvRow, nullptr, "SetDeathTime", nullptr, nullptr, sizeof(Z_Construct_UFunction_UEnemyCsvRow_SetDeathTime_Statics::EnemyCsvRow_eventSetDeathTime_Parms), Z_Construct_UFunction_UEnemyCsvRow_SetDeathTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_SetDeathTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnemyCsvRow_SetDeathTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_SetDeathTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnemyCsvRow_SetDeathTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UEnemyCsvRow_SetDeathTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnemyCsvRow_SetEnemyId_Statics
	{
		struct EnemyCsvRow_eventSetEnemyId_Parms
		{
			int32 inEnemyId;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_inEnemyId;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UEnemyCsvRow_SetEnemyId_Statics::NewProp_inEnemyId = { "inEnemyId", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(EnemyCsvRow_eventSetEnemyId_Parms, inEnemyId), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnemyCsvRow_SetEnemyId_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnemyCsvRow_SetEnemyId_Statics::NewProp_inEnemyId,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnemyCsvRow_SetEnemyId_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnemyCsvRow_SetEnemyId_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnemyCsvRow, nullptr, "SetEnemyId", nullptr, nullptr, sizeof(Z_Construct_UFunction_UEnemyCsvRow_SetEnemyId_Statics::EnemyCsvRow_eventSetEnemyId_Parms), Z_Construct_UFunction_UEnemyCsvRow_SetEnemyId_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_SetEnemyId_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnemyCsvRow_SetEnemyId_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_SetEnemyId_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnemyCsvRow_SetEnemyId()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UEnemyCsvRow_SetEnemyId_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnemyCsvRow_SetEnemyType_Statics
	{
		struct EnemyCsvRow_eventSetEnemyType_Parms
		{
			FString inEnemyType;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_inEnemyType;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UEnemyCsvRow_SetEnemyType_Statics::NewProp_inEnemyType = { "inEnemyType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(EnemyCsvRow_eventSetEnemyType_Parms, inEnemyType), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnemyCsvRow_SetEnemyType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnemyCsvRow_SetEnemyType_Statics::NewProp_inEnemyType,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnemyCsvRow_SetEnemyType_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnemyCsvRow_SetEnemyType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnemyCsvRow, nullptr, "SetEnemyType", nullptr, nullptr, sizeof(Z_Construct_UFunction_UEnemyCsvRow_SetEnemyType_Statics::EnemyCsvRow_eventSetEnemyType_Parms), Z_Construct_UFunction_UEnemyCsvRow_SetEnemyType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_SetEnemyType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnemyCsvRow_SetEnemyType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_SetEnemyType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnemyCsvRow_SetEnemyType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UEnemyCsvRow_SetEnemyType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnemyCsvRow_SetRoundNumber_Statics
	{
		struct EnemyCsvRow_eventSetRoundNumber_Parms
		{
			int32 inRoundNumber;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_inRoundNumber;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UEnemyCsvRow_SetRoundNumber_Statics::NewProp_inRoundNumber = { "inRoundNumber", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(EnemyCsvRow_eventSetRoundNumber_Parms, inRoundNumber), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnemyCsvRow_SetRoundNumber_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnemyCsvRow_SetRoundNumber_Statics::NewProp_inRoundNumber,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnemyCsvRow_SetRoundNumber_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnemyCsvRow_SetRoundNumber_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnemyCsvRow, nullptr, "SetRoundNumber", nullptr, nullptr, sizeof(Z_Construct_UFunction_UEnemyCsvRow_SetRoundNumber_Statics::EnemyCsvRow_eventSetRoundNumber_Parms), Z_Construct_UFunction_UEnemyCsvRow_SetRoundNumber_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_SetRoundNumber_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnemyCsvRow_SetRoundNumber_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_SetRoundNumber_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnemyCsvRow_SetRoundNumber()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UEnemyCsvRow_SetRoundNumber_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UEnemyCsvRow_SetSpawnTime_Statics
	{
		struct EnemyCsvRow_eventSetSpawnTime_Parms
		{
			FDateTime inSpawnTime;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_inSpawnTime;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UEnemyCsvRow_SetSpawnTime_Statics::NewProp_inSpawnTime = { "inSpawnTime", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(EnemyCsvRow_eventSetSpawnTime_Parms, inSpawnTime), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnemyCsvRow_SetSpawnTime_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnemyCsvRow_SetSpawnTime_Statics::NewProp_inSpawnTime,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnemyCsvRow_SetSpawnTime_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnemyCsvRow_SetSpawnTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnemyCsvRow, nullptr, "SetSpawnTime", nullptr, nullptr, sizeof(Z_Construct_UFunction_UEnemyCsvRow_SetSpawnTime_Statics::EnemyCsvRow_eventSetSpawnTime_Parms), Z_Construct_UFunction_UEnemyCsvRow_SetSpawnTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_SetSpawnTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnemyCsvRow_SetSpawnTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnemyCsvRow_SetSpawnTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnemyCsvRow_SetSpawnTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UEnemyCsvRow_SetSpawnTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UEnemyCsvRow);
	UClass* Z_Construct_UClass_UEnemyCsvRow_NoRegister()
	{
		return UEnemyCsvRow::StaticClass();
	}
	struct Z_Construct_UClass_UEnemyCsvRow_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_enemyId_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_enemyId;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_roundNumber_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_roundNumber;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_spawnTime_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_spawnTime;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_deathTime_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_deathTime;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_spawnPoint_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_spawnPoint;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_deathPoint_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_deathPoint;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_enemyType_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_enemyType;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEnemyCsvRow_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCsvTableRow,
		(UObject* (*)())Z_Construct_UPackage__Script_StatisticCollector,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEnemyCsvRow_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEnemyCsvRow_GetDeathTime, "GetDeathTime" }, // 941105519
		{ &Z_Construct_UFunction_UEnemyCsvRow_GetEnemyId, "GetEnemyId" }, // 4206687321
		{ &Z_Construct_UFunction_UEnemyCsvRow_GetEnemyType, "GetEnemyType" }, // 190719696
		{ &Z_Construct_UFunction_UEnemyCsvRow_GetRoundNumber, "GetRoundNumber" }, // 1377844259
		{ &Z_Construct_UFunction_UEnemyCsvRow_GetSpawnTime, "GetSpawnTime" }, // 2865865244
		{ &Z_Construct_UFunction_UEnemyCsvRow_SetDeathTime, "SetDeathTime" }, // 2686036291
		{ &Z_Construct_UFunction_UEnemyCsvRow_SetEnemyId, "SetEnemyId" }, // 1268070440
		{ &Z_Construct_UFunction_UEnemyCsvRow_SetEnemyType, "SetEnemyType" }, // 1705882425
		{ &Z_Construct_UFunction_UEnemyCsvRow_SetRoundNumber, "SetRoundNumber" }, // 811349398
		{ &Z_Construct_UFunction_UEnemyCsvRow_SetSpawnTime, "SetSpawnTime" }, // 1924120050
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnemyCsvRow_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "EnemyCsvRow.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_enemyId_MetaData[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_enemyId = { "enemyId", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UEnemyCsvRow, enemyId), METADATA_PARAMS(Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_enemyId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_enemyId_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_roundNumber_MetaData[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_roundNumber = { "roundNumber", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UEnemyCsvRow, roundNumber), METADATA_PARAMS(Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_roundNumber_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_roundNumber_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_spawnTime_MetaData[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_spawnTime = { "spawnTime", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UEnemyCsvRow, spawnTime), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_spawnTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_spawnTime_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_deathTime_MetaData[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_deathTime = { "deathTime", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UEnemyCsvRow, deathTime), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_deathTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_deathTime_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_spawnPoint_MetaData[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_spawnPoint = { "spawnPoint", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UEnemyCsvRow, spawnPoint), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_spawnPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_spawnPoint_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_deathPoint_MetaData[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_deathPoint = { "deathPoint", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UEnemyCsvRow, deathPoint), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_deathPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_deathPoint_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_enemyType_MetaData[] = {
		{ "ModuleRelativePath", "Public/EnemyCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_enemyType = { "enemyType", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UEnemyCsvRow, enemyType), METADATA_PARAMS(Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_enemyType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_enemyType_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEnemyCsvRow_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_enemyId,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_roundNumber,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_spawnTime,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_deathTime,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_spawnPoint,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_deathPoint,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnemyCsvRow_Statics::NewProp_enemyType,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEnemyCsvRow_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEnemyCsvRow>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UEnemyCsvRow_Statics::ClassParams = {
		&UEnemyCsvRow::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UEnemyCsvRow_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UEnemyCsvRow_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEnemyCsvRow_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEnemyCsvRow_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEnemyCsvRow()
	{
		if (!Z_Registration_Info_UClass_UEnemyCsvRow.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UEnemyCsvRow.OuterSingleton, Z_Construct_UClass_UEnemyCsvRow_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UEnemyCsvRow.OuterSingleton;
	}
	template<> STATISTICCOLLECTOR_API UClass* StaticClass<UEnemyCsvRow>()
	{
		return UEnemyCsvRow::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEnemyCsvRow);
	UEnemyCsvRow::~UEnemyCsvRow() {}
	struct Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UEnemyCsvRow, UEnemyCsvRow::StaticClass, TEXT("UEnemyCsvRow"), &Z_Registration_Info_UClass_UEnemyCsvRow, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UEnemyCsvRow), 3201702449U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_1045625140(TEXT("/Script/StatisticCollector"),
		Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_EnemyCsvRow_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
