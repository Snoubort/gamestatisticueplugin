// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StatisticCollector/Public/PlayerCsvRow.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlayerCsvRow() {}
// Cross Module References
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_UCsvTableRow();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_UPlayerCsvRow();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_UPlayerCsvRow_NoRegister();
	UPackage* Z_Construct_UPackage__Script_StatisticCollector();
// End Cross Module References
	DEFINE_FUNCTION(UPlayerCsvRow::execGetHp)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetHp();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPlayerCsvRow::execSetHp)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_newHp);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetHp(Z_Param_newHp);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPlayerCsvRow::execGetRoundNumber)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetRoundNumber();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPlayerCsvRow::execSetRoundNum)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_newRoundNumber);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetRoundNum(Z_Param_newRoundNumber);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPlayerCsvRow::execGetId)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetId();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPlayerCsvRow::execSetId)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_id);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetId(Z_Param_id);
		P_NATIVE_END;
	}
	void UPlayerCsvRow::StaticRegisterNativesUPlayerCsvRow()
	{
		UClass* Class = UPlayerCsvRow::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetHp", &UPlayerCsvRow::execGetHp },
			{ "GetId", &UPlayerCsvRow::execGetId },
			{ "GetRoundNumber", &UPlayerCsvRow::execGetRoundNumber },
			{ "SetHp", &UPlayerCsvRow::execSetHp },
			{ "SetId", &UPlayerCsvRow::execSetId },
			{ "SetRoundNum", &UPlayerCsvRow::execSetRoundNum },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPlayerCsvRow_GetHp_Statics
	{
		struct PlayerCsvRow_eventGetHp_Parms
		{
			int32 ReturnValue;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UPlayerCsvRow_GetHp_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PlayerCsvRow_eventGetHp_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPlayerCsvRow_GetHp_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlayerCsvRow_GetHp_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlayerCsvRow_GetHp_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PlayerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlayerCsvRow_GetHp_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlayerCsvRow, nullptr, "GetHp", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPlayerCsvRow_GetHp_Statics::PlayerCsvRow_eventGetHp_Parms), Z_Construct_UFunction_UPlayerCsvRow_GetHp_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlayerCsvRow_GetHp_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlayerCsvRow_GetHp_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlayerCsvRow_GetHp_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlayerCsvRow_GetHp()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlayerCsvRow_GetHp_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPlayerCsvRow_GetId_Statics
	{
		struct PlayerCsvRow_eventGetId_Parms
		{
			int32 ReturnValue;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UPlayerCsvRow_GetId_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PlayerCsvRow_eventGetId_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPlayerCsvRow_GetId_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlayerCsvRow_GetId_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlayerCsvRow_GetId_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PlayerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlayerCsvRow_GetId_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlayerCsvRow, nullptr, "GetId", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPlayerCsvRow_GetId_Statics::PlayerCsvRow_eventGetId_Parms), Z_Construct_UFunction_UPlayerCsvRow_GetId_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlayerCsvRow_GetId_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlayerCsvRow_GetId_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlayerCsvRow_GetId_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlayerCsvRow_GetId()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlayerCsvRow_GetId_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPlayerCsvRow_GetRoundNumber_Statics
	{
		struct PlayerCsvRow_eventGetRoundNumber_Parms
		{
			int32 ReturnValue;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UPlayerCsvRow_GetRoundNumber_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PlayerCsvRow_eventGetRoundNumber_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPlayerCsvRow_GetRoundNumber_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlayerCsvRow_GetRoundNumber_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlayerCsvRow_GetRoundNumber_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PlayerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlayerCsvRow_GetRoundNumber_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlayerCsvRow, nullptr, "GetRoundNumber", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPlayerCsvRow_GetRoundNumber_Statics::PlayerCsvRow_eventGetRoundNumber_Parms), Z_Construct_UFunction_UPlayerCsvRow_GetRoundNumber_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlayerCsvRow_GetRoundNumber_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlayerCsvRow_GetRoundNumber_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlayerCsvRow_GetRoundNumber_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlayerCsvRow_GetRoundNumber()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlayerCsvRow_GetRoundNumber_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPlayerCsvRow_SetHp_Statics
	{
		struct PlayerCsvRow_eventSetHp_Parms
		{
			int32 newHp;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_newHp;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UPlayerCsvRow_SetHp_Statics::NewProp_newHp = { "newHp", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PlayerCsvRow_eventSetHp_Parms, newHp), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPlayerCsvRow_SetHp_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlayerCsvRow_SetHp_Statics::NewProp_newHp,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlayerCsvRow_SetHp_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PlayerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlayerCsvRow_SetHp_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlayerCsvRow, nullptr, "SetHp", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPlayerCsvRow_SetHp_Statics::PlayerCsvRow_eventSetHp_Parms), Z_Construct_UFunction_UPlayerCsvRow_SetHp_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlayerCsvRow_SetHp_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlayerCsvRow_SetHp_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlayerCsvRow_SetHp_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlayerCsvRow_SetHp()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlayerCsvRow_SetHp_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPlayerCsvRow_SetId_Statics
	{
		struct PlayerCsvRow_eventSetId_Parms
		{
			int32 id;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_id;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UPlayerCsvRow_SetId_Statics::NewProp_id = { "id", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PlayerCsvRow_eventSetId_Parms, id), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPlayerCsvRow_SetId_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlayerCsvRow_SetId_Statics::NewProp_id,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlayerCsvRow_SetId_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PlayerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlayerCsvRow_SetId_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlayerCsvRow, nullptr, "SetId", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPlayerCsvRow_SetId_Statics::PlayerCsvRow_eventSetId_Parms), Z_Construct_UFunction_UPlayerCsvRow_SetId_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlayerCsvRow_SetId_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlayerCsvRow_SetId_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlayerCsvRow_SetId_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlayerCsvRow_SetId()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlayerCsvRow_SetId_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPlayerCsvRow_SetRoundNum_Statics
	{
		struct PlayerCsvRow_eventSetRoundNum_Parms
		{
			int32 newRoundNumber;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_newRoundNumber;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UPlayerCsvRow_SetRoundNum_Statics::NewProp_newRoundNumber = { "newRoundNumber", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(PlayerCsvRow_eventSetRoundNum_Parms, newRoundNumber), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPlayerCsvRow_SetRoundNum_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPlayerCsvRow_SetRoundNum_Statics::NewProp_newRoundNumber,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPlayerCsvRow_SetRoundNum_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/PlayerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UPlayerCsvRow_SetRoundNum_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPlayerCsvRow, nullptr, "SetRoundNum", nullptr, nullptr, sizeof(Z_Construct_UFunction_UPlayerCsvRow_SetRoundNum_Statics::PlayerCsvRow_eventSetRoundNum_Parms), Z_Construct_UFunction_UPlayerCsvRow_SetRoundNum_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlayerCsvRow_SetRoundNum_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPlayerCsvRow_SetRoundNum_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPlayerCsvRow_SetRoundNum_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPlayerCsvRow_SetRoundNum()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UPlayerCsvRow_SetRoundNum_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UPlayerCsvRow);
	UClass* Z_Construct_UClass_UPlayerCsvRow_NoRegister()
	{
		return UPlayerCsvRow::StaticClass();
	}
	struct Z_Construct_UClass_UPlayerCsvRow_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_playerID_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_playerID;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_roundNum_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_roundNum;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_hp_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_hp;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlayerCsvRow_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCsvTableRow,
		(UObject* (*)())Z_Construct_UPackage__Script_StatisticCollector,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPlayerCsvRow_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPlayerCsvRow_GetHp, "GetHp" }, // 1334408269
		{ &Z_Construct_UFunction_UPlayerCsvRow_GetId, "GetId" }, // 2488784980
		{ &Z_Construct_UFunction_UPlayerCsvRow_GetRoundNumber, "GetRoundNumber" }, // 3393738038
		{ &Z_Construct_UFunction_UPlayerCsvRow_SetHp, "SetHp" }, // 1852605115
		{ &Z_Construct_UFunction_UPlayerCsvRow_SetId, "SetId" }, // 1384335135
		{ &Z_Construct_UFunction_UPlayerCsvRow_SetRoundNum, "SetRoundNum" }, // 579777314
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerCsvRow_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "PlayerCsvRow.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/PlayerCsvRow.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerCsvRow_Statics::NewProp_playerID_MetaData[] = {
		{ "ModuleRelativePath", "Public/PlayerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UPlayerCsvRow_Statics::NewProp_playerID = { "playerID", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UPlayerCsvRow, playerID), METADATA_PARAMS(Z_Construct_UClass_UPlayerCsvRow_Statics::NewProp_playerID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerCsvRow_Statics::NewProp_playerID_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerCsvRow_Statics::NewProp_roundNum_MetaData[] = {
		{ "ModuleRelativePath", "Public/PlayerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UPlayerCsvRow_Statics::NewProp_roundNum = { "roundNum", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UPlayerCsvRow, roundNum), METADATA_PARAMS(Z_Construct_UClass_UPlayerCsvRow_Statics::NewProp_roundNum_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerCsvRow_Statics::NewProp_roundNum_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerCsvRow_Statics::NewProp_hp_MetaData[] = {
		{ "ModuleRelativePath", "Public/PlayerCsvRow.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UPlayerCsvRow_Statics::NewProp_hp = { "hp", nullptr, (EPropertyFlags)0x0040000000000000, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UPlayerCsvRow, hp), METADATA_PARAMS(Z_Construct_UClass_UPlayerCsvRow_Statics::NewProp_hp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerCsvRow_Statics::NewProp_hp_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPlayerCsvRow_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlayerCsvRow_Statics::NewProp_playerID,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlayerCsvRow_Statics::NewProp_roundNum,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlayerCsvRow_Statics::NewProp_hp,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlayerCsvRow_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPlayerCsvRow>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UPlayerCsvRow_Statics::ClassParams = {
		&UPlayerCsvRow::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPlayerCsvRow_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerCsvRow_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPlayerCsvRow_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerCsvRow_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlayerCsvRow()
	{
		if (!Z_Registration_Info_UClass_UPlayerCsvRow.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UPlayerCsvRow.OuterSingleton, Z_Construct_UClass_UPlayerCsvRow_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UPlayerCsvRow.OuterSingleton;
	}
	template<> STATISTICCOLLECTOR_API UClass* StaticClass<UPlayerCsvRow>()
	{
		return UPlayerCsvRow::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlayerCsvRow);
	UPlayerCsvRow::~UPlayerCsvRow() {}
	struct Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_PlayerCsvRow_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_PlayerCsvRow_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UPlayerCsvRow, UPlayerCsvRow::StaticClass, TEXT("UPlayerCsvRow"), &Z_Registration_Info_UClass_UPlayerCsvRow, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UPlayerCsvRow), 3607650268U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_PlayerCsvRow_h_2403393066(TEXT("/Script/StatisticCollector"),
		Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_PlayerCsvRow_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_PlayerCsvRow_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
