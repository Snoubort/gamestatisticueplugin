// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StatisticCollector/Public/CsvTableRow.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCsvTableRow() {}
// Cross Module References
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_UCsvTableRow();
	STATISTICCOLLECTOR_API UClass* Z_Construct_UClass_UCsvTableRow_NoRegister();
	STATISTICCOLLECTOR_API UEnum* Z_Construct_UEnum_StatisticCollector_ECsvRowTypes();
	UPackage* Z_Construct_UPackage__Script_StatisticCollector();
// End Cross Module References
	DEFINE_FUNCTION(UCsvTableRow::execGetHeaderString)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetHeaderString();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCsvTableRow::execGetDataString)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetDataString();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCsvTableRow::execGetRowType)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ECsvRowTypes*)Z_Param__Result=P_THIS->GetRowType();
		P_NATIVE_END;
	}
	void UCsvTableRow::StaticRegisterNativesUCsvTableRow()
	{
		UClass* Class = UCsvTableRow::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDataString", &UCsvTableRow::execGetDataString },
			{ "GetHeaderString", &UCsvTableRow::execGetHeaderString },
			{ "GetRowType", &UCsvTableRow::execGetRowType },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCsvTableRow_GetDataString_Statics
	{
		struct CsvTableRow_eventGetDataString_Parms
		{
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCsvTableRow_GetDataString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CsvTableRow_eventGetDataString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCsvTableRow_GetDataString_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCsvTableRow_GetDataString_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCsvTableRow_GetDataString_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CsvTableRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCsvTableRow_GetDataString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCsvTableRow, nullptr, "GetDataString", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCsvTableRow_GetDataString_Statics::CsvTableRow_eventGetDataString_Parms), Z_Construct_UFunction_UCsvTableRow_GetDataString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTableRow_GetDataString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCsvTableRow_GetDataString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTableRow_GetDataString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCsvTableRow_GetDataString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCsvTableRow_GetDataString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCsvTableRow_GetHeaderString_Statics
	{
		struct CsvTableRow_eventGetHeaderString_Parms
		{
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCsvTableRow_GetHeaderString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CsvTableRow_eventGetHeaderString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCsvTableRow_GetHeaderString_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCsvTableRow_GetHeaderString_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCsvTableRow_GetHeaderString_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CsvTableRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCsvTableRow_GetHeaderString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCsvTableRow, nullptr, "GetHeaderString", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCsvTableRow_GetHeaderString_Statics::CsvTableRow_eventGetHeaderString_Parms), Z_Construct_UFunction_UCsvTableRow_GetHeaderString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTableRow_GetHeaderString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCsvTableRow_GetHeaderString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTableRow_GetHeaderString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCsvTableRow_GetHeaderString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCsvTableRow_GetHeaderString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCsvTableRow_GetRowType_Statics
	{
		struct CsvTableRow_eventGetRowType_Parms
		{
			ECsvRowTypes ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UCsvTableRow_GetRowType_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UCsvTableRow_GetRowType_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(CsvTableRow_eventGetRowType_Parms, ReturnValue), Z_Construct_UEnum_StatisticCollector_ECsvRowTypes, METADATA_PARAMS(nullptr, 0) }; // 1594374167
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCsvTableRow_GetRowType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCsvTableRow_GetRowType_Statics::NewProp_ReturnValue_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCsvTableRow_GetRowType_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCsvTableRow_GetRowType_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/CsvTableRow.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UCsvTableRow_GetRowType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCsvTableRow, nullptr, "GetRowType", nullptr, nullptr, sizeof(Z_Construct_UFunction_UCsvTableRow_GetRowType_Statics::CsvTableRow_eventGetRowType_Parms), Z_Construct_UFunction_UCsvTableRow_GetRowType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTableRow_GetRowType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCsvTableRow_GetRowType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCsvTableRow_GetRowType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCsvTableRow_GetRowType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UCsvTableRow_GetRowType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UCsvTableRow);
	UClass* Z_Construct_UClass_UCsvTableRow_NoRegister()
	{
		return UCsvTableRow::StaticClass();
	}
	struct Z_Construct_UClass_UCsvTableRow_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FBytePropertyParams NewProp_rowType_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_rowType_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_rowType;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCsvTableRow_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_StatisticCollector,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCsvTableRow_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCsvTableRow_GetDataString, "GetDataString" }, // 2773171184
		{ &Z_Construct_UFunction_UCsvTableRow_GetHeaderString, "GetHeaderString" }, // 2622377264
		{ &Z_Construct_UFunction_UCsvTableRow_GetRowType, "GetRowType" }, // 1725090455
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCsvTableRow_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "CsvTableRow.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/CsvTableRow.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCsvTableRow_Statics::NewProp_rowType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCsvTableRow_Statics::NewProp_rowType_MetaData[] = {
		{ "ModuleRelativePath", "Public/CsvTableRow.h" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCsvTableRow_Statics::NewProp_rowType = { "rowType", nullptr, (EPropertyFlags)0x0020080000000000, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(UCsvTableRow, rowType), Z_Construct_UEnum_StatisticCollector_ECsvRowTypes, METADATA_PARAMS(Z_Construct_UClass_UCsvTableRow_Statics::NewProp_rowType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCsvTableRow_Statics::NewProp_rowType_MetaData)) }; // 1594374167
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCsvTableRow_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCsvTableRow_Statics::NewProp_rowType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCsvTableRow_Statics::NewProp_rowType,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCsvTableRow_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCsvTableRow>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UCsvTableRow_Statics::ClassParams = {
		&UCsvTableRow::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCsvTableRow_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCsvTableRow_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCsvTableRow_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCsvTableRow_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCsvTableRow()
	{
		if (!Z_Registration_Info_UClass_UCsvTableRow.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UCsvTableRow.OuterSingleton, Z_Construct_UClass_UCsvTableRow_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UCsvTableRow.OuterSingleton;
	}
	template<> STATISTICCOLLECTOR_API UClass* StaticClass<UCsvTableRow>()
	{
		return UCsvTableRow::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCsvTableRow);
	UCsvTableRow::~UCsvTableRow() {}
	struct Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UCsvTableRow, UCsvTableRow::StaticClass, TEXT("UCsvTableRow"), &Z_Registration_Info_UClass_UCsvTableRow, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UCsvTableRow), 2611805874U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_1115953038(TEXT("/Script/StatisticCollector"),
		Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_StatisticTest_Plugins_StatisticCollector_Source_StatisticCollector_Public_CsvTableRow_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
